"""Different matplotlib style options for customizing plots.

To allow for more flexibility, we provide these as dictionaries.  (This allows one to
use hashes for example, which are not valid in an mplstyle file.)

Use as you would styles:

>>> import matplotlib.pyplot as plt
>>> import mpl_styles
>>> plt.style.use(mpl_styles.nature)

Or, better, use it locally:

>>> with plt.style.context(mpl_styles.nature, after_reset=True):
...     plt.plot([1,2], [1,2])

Note: plots must be displayed and/or saved **within the context**, otherwise some of the
customizations may not take place (especially LaTeX modifications).  For this reason, we
provide default "show" and "savefig" arguments to the context which do this before exiting.

* https://github.com/matplotlib/matplotlib/issues/13431
* https://github.com/matplotlib/matplotlib/issues/17931

etc.
"""
import contextlib

import numpy as np
from matplotlib import pyplot as plt
import warnings


def get_constants(**kw):
    """Return a constants dict calculating everything in terms of various units.

    Everything is ultimately expressed in inches.
    """
    constants = dict(
        pt_per_inch=72.27,
        mm_per_inch=127 / 5,
        golden_mean=2 / (1 + np.sqrt(5)),
    )

    constants.update(kw)

    _suffix = "_per_inch"
    units = [
        _key[:-len(_suffix)] for _key in constants if _key.endswith(_suffix)
    ]
    for unit in units:
        _suffix = f"_{unit}"
        for key_ in [_k for _k in constants if _k.endswith(_suffix)]:
            key = key_[:-len(_suffix)]
            if key in constants:
                raise ValueError(
                    f"{key}={constants[key]} defined, but also got {key_}={constants[key_]}"
                )
            constants[key] = constants[key_] / constants[f"{unit}_per_inch"]
    return constants


@contextlib.contextmanager
def base_style(
    figuretype="figure",
    height="golden_mean*width",
    base="seaborn-paper",
    after_reset=True,
    style_dict=None,
    fname=None,
    savefig=True,
    savefig_kw=None,
    show=True,
    close_all=False,
    constants=None,
):
    """Default style context.

    Warning: For some parameters like LaTeX fonts etc., the figure MUST be
    displayed (`plt.show()`) or saved (`plt.savefig()`) within the context.
    We do this at the end of the context here by default.

    Arguments
    ---------
    figuretype : str
        Figure type.  For convenience, we currently match these with the latex command
        for the `revtex` classes.  Thus, we have the following options:

        'figure': Standard single-column figure of width `columnwidth`.
        'figure*': Full-page figure of width `fullwidth`.

    fname : None, str
        Name for figure file.  Passed to `plt.savefig()`.
    savefig : bool, str
        If `True` or a filename, then the figure will be saved.
    savefig_kw : dict
        Arguments for plt.savefig(bbox_inches="tight",  etc.).
    show : bool
        If True, then call `plt.show()` in the context.
    close_all : bool
        If `True`, then call `plt.close('all')` after saving to conserve resources.
    constants : dict
        Dictionary of constant values.
    """
    if constants is None:
        constants = dict(
            columnwidth_pt=
            246.0,  # \begin{figure}\showthe\columnwidth\end{figure}
            fullwidth_pt=510.0,  # \begin{figure*}\showthe\textwidth\end{figure*}
            marginwidth_pt=
            144.0,  # \begin{marginfigure}\showthe\textwidth\end{marginfigure}
        )

    constants = get_constants(**constants)
    width = {"figure": "columnwidth", "figure*": "fullwidth"}[figuretype]

    constants["width"] = eval(str(width), constants)
    constants["height"] = eval(str(height), constants)

    style = {}
    if style_dict:
        style.update(style_dict)
    style["figure.figsize"] = (constants["width"], constants["height"])
    with plt.style.context([base, style], after_reset=after_reset):
        yield constants

        # The following need to be done in the context, especially
        # for latex fonts to work properly
        # https://github.com/matplotlib/matplotlib/issues/13431
        # https://github.com/matplotlib/matplotlib/issues/17931
        if fname and savefig:
            if not savefig_kw:
                savefig_kw = {}
            plt.savefig(fname=fname, **savefig_kw)
        if show:
            plt.show()
        if close_all:
            plt.close("all")


######################################################################
# Nature Communications
nature_dict = {
    "text.usetex": True,
    "axes.linewidth": 1.0,
    "axes.edgecolor": "grey",
    "axes.grid": True,
    "axes.axisbelow": True,
    "grid.linestyle": "-",
    "grid.linewidth": 1.0,
    "grid.color": "WhiteSmoke",
    "grid.alpha": 0.3,
    "ytick.direction": "inout",
    "xtick.direction": "inout",
    "xtick.major.size": 2,
    "xtick.minor.size": 1,
    "ytick.major.size": 2,
    "ytick.minor.size": 1,
    "xtick.bottom": True,
    "xtick.top": True,
    "ytick.left": True,
    "ytick.right": True,
    "lines.linewidth": 1,
    "font.family": "sans-serif",
    "text.latex.preamble": "".join([
        # r"\usepackage[utf8]{inputenc}",  # Provided by matplotlib with pdflatex backend
        r"\usepackage[greek,english]{babel}",  # Allow greek unicode letters.
        # r"\usepackage{newtxmath}",  # http://tex.stackexchange.com/a/197874/6903
        # r"\usepackage{newtxsf}",
        r"\usepackage{sansmathfonts}",  # http://tex.stackexchange.com/a/197874/6903
        r"\usepackage[scaled=0.86]{helvet}",  # Trial and error for correct size...
        r"\renewcommand{\rmdefault}{\sfdefault}",
        r"\usepackage{amsmath}",
        r"\usepackage{siunitx}",
        r"\sisetup{mode=text, detect-all=true, detect-mode=false}",
    ]),
    "ps.fonttype":  42,
    "grid.alpha": 0.05,
}


@contextlib.contextmanager
def nature_style(
    figuretype="figure",
    *argv,
    style_dict=None,
    **kwargs,
):
    """Context for plotting in Nature Communications."""
    style_dict_ = dict(nature_dict)
    if style_dict:
        style_dict_.update(style_dict)

    constants = dict(figuretype=figuretype,
                     columnwidth_mm=88,
                     fullwidth_mm=180)
    with base_style(figuretype,
                    *argv,
                    style_dict=style_dict_,
                    constants=constants,
                    **kwargs) as constants:
        height_mm = constants["height"] * constants["mm_per_inch"]
        if figuretype == "figure":
            if height_mm > 220:
                warnings.warn(
                    f"Figure height {height_mm}>220mm too large for Nature.")
            elif height_mm > 180:
                warnings.warn(
                    f"Figure height {height_mm}>180mm: make sure caption < 50 words."
                )
            elif height_mm > 130:
                warnings.warn(
                    f"Figure height {height_mm}>120mm: make sure caption < 150 words."
                )
            else:
                warnings.warn("Make sure caption < 300 words.")
        elif figuretype == "figure*":
            if height_mm > 225:
                warnings.warn(
                    f"Figure height {height_mm}>225mm too large for Nature.")
            elif height_mm > 210:
                warnings.warn(
                    f"Figure height {height_mm}>210mm: make sure caption < 50 words."
                )
            elif height_mm > 185:
                warnings.warn(
                    f"Figure height {height_mm}>185mm: make sure caption < 150 words."
                )
            else:
                warnings.warn("Make sure caption < 300 words.")
        else:
            warnings.warn(f"Unknown figuretype={figuretype}.")

        yield constants


###############
# APS Journals
aps_dict = {
    "text.usetex": True,
    "axes.linewidth": 1.0,
    "axes.edgecolor": "grey",
    "axes.grid": True,
    "axes.axisbelow": True,
    "grid.linestyle": "-",
    "grid.linewidth": 1.0,
    "grid.color": "WhiteSmoke",
    "grid.alpha": 0.3,
    "ytick.direction": "inout",
    "xtick.direction": "inout",
    "xtick.major.size": 2,
    "xtick.minor.size": 1,
    "ytick.major.size": 2,
    "ytick.minor.size": 1,
    "xtick.bottom": True,
    "xtick.top": True,
    "ytick.left": True,
    "ytick.right": True,
    "lines.linewidth": 1,
    "font.family": "sans-serif",
    "text.latex.preamble": "".join([
        # r"\usepackage[utf8]{inputenc}",  # Provided by matplotlib with pdflatex backend
        r"\usepackage[greek,english]{babel}",  # Allow greek unicode letters.
        r"\usepackage{newtxmath}",  # http://tex.stackexchange.com/a/197874/6903
        r"\usepackage{amsmath}",
        r"\usepackage{siunitx}",
        r"\sisetup{mode=text, detect-all=true, detect-mode=false}",
    ]),
    "ps.fonttype":  42,
    "grid.alpha": 0.5,
}

@contextlib.contextmanager
def aps_style(
    figuretype="figure",
    *argv,
    style_dict=None,
    **kwargs,
):
    """Context for plotting in APS journals."""
    style_dict_ = dict(aps_dict)
    if style_dict:
        style_dict_.update(style_dict)

    with base_style(figuretype, *argv, style_dict=style_dict_,
                    **kwargs) as constants:
        yield constants
