import os

import numpy as np


class Units(object):
    #hbar = float(os.environ.get('Units_hbar', 1.0))
    #micron = float(os.environ.get('Units_micron', 1.0))
    #u = float(os.environ.get('Units_u', 1.0))     # amu
    #G = float(os.environ.get('Units_G', 1.0))
    
    hbar = 1.0
    micron = 1.0
    u = 1.0
    G = 1.0

    mm = 1e3*micron
    cm = 1e4*micron
    nm = 1e-3*micron
    meter = 1e3*mm

    kg = u/1.660539040e-27

    m_F = m_Li = 6.015122795*u
    m_B = m_Yb = 173.9388621*u
    m_Na = 22.9897692809*u
    m_FB = 1./(1./m_F + 1./m_B)    # Reduced mass
    m_P = 1.00727647*u             # mass of proton
    m_e = 0.00054858*u             # mass of electron
    a_Yb_Yb = 5.5*nm               # from Kitagawa, PRA 77, 012719 (2008)

    a_B = 5.2917721054980885238e-5*micron

    a_Li_Yb = 2*15*a_B             # Green et. al. arXiv:1903.00603(2019)
    a_s = 45.1591*a_B              # singlet scattering length for Li6
                                   # from Schunck, PRA 71, 045601 (2005)
    a_dd = 0.6*a_s                 # from Petrov, PRL 93, 090404 (2004),
                                   # dimer-dimer scattering length for Li6
    a_db = 3.87*a_Li_Yb            # from Cui, PRA 90, 041603(R)

    g_Yb_Yb = 4*np.pi*hbar**2 * a_Yb_Yb / m_Yb

    # hbar/micron^2/u = 63507.799258914903398 Hz
    Hz = hbar/micron**2/u/63507.799258914903398
    kHz = 1e3*Hz
    s = 1./Hz
    ms = 1e-3*s

    watt = kg*meter**2/s**3
    amp = 1.0*1e-4*kg/G/s**2      # current

    e = 1.6e-19*amp*s             # Charge of electron
    nuclear_magneton = e*hbar/2.0/m_P
    bohr_magneton = e*hbar/2.0/m_e
    mu_Yb = 0.0*nuclear_magneton
    mu_Li = 1.0*bohr_magneton

    gravity = 9.81*meter/s**2

    alpha_Yb = 139*1.64877772754e-41*s**4*amp**2/kg # from https://doi.org/10.1080/00268976.2018.1535143
                                                    # static polarizability

    # Parameters describing the paired Fermi gas
    xi = 0.3742                 # Bertsch parameter
    zeta = 0.901                # The contact
    aDD_a = 0.6                 # dimer-dimer scattering length (aDD)/singlet scattering length for Li6

    B_RESONANCE_GAUSS = 8.32178498e+02 * G # Unitarity for Li6


u = Units()