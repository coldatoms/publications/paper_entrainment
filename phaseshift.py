"""
Usage:
We provide a units class for the calculations. The main calculation
is done in the get_phase_shift_percent() function.
Minimal usage can be done in the following way:

import phaseshift.py as ps

# Get the parameter sets, current (args_c) and future (args_f)
args_c, args_f = ps.get_args()

# Call the function with appropriate arguments:

phase_shift_current_params = ps.get_phase_shift_percent(**args_c)
phase_shift_future_params = ps.get_phase_shift_percent(**args_f)

"""
from collections import namedtuple
import os

import numpy as np

from scipy.integrate import quad
from scipy.optimize import brentq

## units is a script where we set the units for computation.

## li_yb is a script where we numerically implement the equation of 
## state for Li-6 unitary Fermi gas which smoothly connects the
## BEC limit to unitarity. Please see main text for references.

from phaseshift_code_submission.units import u
from phaseshift_code_submission.li_yb import EoSUFG    #, Units
eos_F = EoSUFG(b=0.25)

# this 'b' value fits the QMC data. Please see references in the text.
hbar = u.hbar

## For the detailed description of the parameters, please read the docstring of
## the get_phase_shift_percent() function below. That function does the main
## phase shift calculation.

## The following arguments were fixed for all of the calculations presented in the
## main text.

class ImmutableDict(dict):
    def __setitem__(self, key, value):
        raise Exception("Can't touch this")
    def __hash__(self):
        return hash(tuple(sorted(self.items())))

params_fixed = ImmutableDict(dict(
    unitary_gamma=True,
    a_DD=None,
    a_bb=u.a_Yb_Yb,      # boson-boson scattering length
    bose_bose=False,
    hbar=u.hbar,
    fudge=1.0,
    B_=None,
    independent_mass=False,
))

## Organization method of the parameter dictionary
## (Typical value 1, Typical value 2, (range over which scaling exponents were calculated))

## The parameters are set here such that the output returns the highest phase shift
## value allowed by the miscibility condition using the mean-field value of the 
## dimer-boson scattering length.

params_var = ImmutableDict(dict(
    B_=(638, 638, (650, 850)),
    R_=(30, 200, (10, 500)),
    Nf=(1e5, 1e6, (1e4, 2e6)),
    Nb=(1e5, 1e6, (1e4, 2e6)),
    alpha_Li_Yb=(6.2, 6.2, (1.0, 8.0)),
    freq_b_=(400, 400, (250, 500)),
    v_vc=(0.15, 0.2, (0.05, 5)),
    a_Db=(3.87*u.a_Li_Yb, 3.87*u.a_Li_Yb,
         (u.a_Li_Yb/2.0, 3.87*u.a_Li_Yb*2)),     # dimer-boson scattering length
    mb_mf=(u.m_Yb/u.m_Li, u.m_Yb/u.m_Li, (1.0, 1.2*u.m_Yb/u.m_Li)),
    mbmf=(u.m_Yb*u.m_Li, u.m_Yb*u.m_Li, (u.m_Yb*u.m_Li/10, 1.2*u.m_Yb*u.m_Li)),
    ))


def get_args(**kw):
    """Returns the current and future values of
    parameters from params_fixed or/and params_var
    dictionary, leaves the range of values out.
    """
    args_c = dict(params_fixed)
    args_f = dict(params_fixed)
    for _p in params_var:
        args_c[_p], args_f[_p] = params_var[_p][:2]
    args_c.update(kw)
    args_f.update(kw)
    return args_c, args_f

def get_args_var(**kw):
    """Returns the current and future values of
    parameters from params_var
    dictionary, leaves the range of values out.
    """
    args_c_var = dict()
    args_f_var = dict()
    for _p in params_var:
        args_c_var[_p], args_f_var[_p] = params_var[_p][:2]
    args_c_var.update(kw)
    args_f_var.update(kw)
    return args_c_var, args_f_var


# Fil:2005 = PhysRevA.72.013616

def get_es(k, ms):
    """Returns the energy spectrum for each component.

    Arguments:
    ----------
    k : lattice vector
    ms : masses of the components
    """
    return np.divide((u.hbar*k)**2/2, ms)

def get_Es(k, ns, ms, gammas):
    """Returns the phonon dispersion relation in the
    absence of interaction between two components.

    Arguments:
    ----------
    k : lattice wave vector
    ns : central densities of components
    ms : masses of components
    gammas : mean-field intra-species interaction coefficient,
             e.g. "g" in GPE
    """
    es = get_es(k=k, ms=ms)
    return np.sqrt(es*(es+2*gammas*ns))

def get_Omegas(k, ns, ms, gammas, gamma_12):
    """Returns the overall phonon dispersion relations.
    With interaction between two components.

    Arguments:
    ----------
    gamma_12 : mean-field inter-species interaction coefficient

    """
    E1, E2 = get_Es(k=k, ns=ns, ms=ms, gammas=gammas)
    m1, m2 = ms
    n1, n2 = ns
    Ep2 = (E1**2 + E2**2)/2.0
    D = np.sqrt((E1**2-E2**2)**2/4 + gamma_12**2*n1*n2*((u.hbar*k)**4/m1/m2))
    return np.sqrt(Ep2 + D), np.sqrt(Ep2 - D)

def rho_dr_integrand(k, ms, ns, gammas, gamma_12):
    """Returns the integrand of eq. 28 from Fil:2005
    at T = 0 but with full mass and scattering length dependence.
    """
    Omega1, Omega2 = Omegas = get_Omegas(k, ns=ns, ms=ms, gammas=gammas, gamma_12=gamma_12)
    m1, m2 = ms
    n1, n2 = ns
    e1, e2 = get_es(k=k, ms=ms)
    return (4/3 * np.sqrt(m1*m2)
            * gamma_12**2 * n1*n2 * (e1*e2)**(3/2)
            / Omega1 / Omega2
            / (Omega1+Omega2)**3) * k**2/2/np.pi**2

def simplified_rho_dr_integrand(k, ms, ns, gammas, gamma_12):
    """Returns the integrand of eq. 28 from Fil:2005
    at T = 0 dropping the cubic dependencies on Omega and e.
    """
    Omega1, Omega2 = Omegas = get_Omegas(k, ns=ns, ms=ms, gammas=gammas, gamma_12=gamma_12)
    m1, m2 = ms
    n1, n2 = ns
    e1, e2 = get_es(k=k, ms=ms)
    return (4/3 * np.sqrt(m1*m2)
            * gamma_12**2 * n1*n2 / Omega1 / Omega2) * k**2/2/np.pi**2 

def get_rho_ent(ms, ns, as_):
    """eq. (28) from Fil:2005

    Arguments:
    ----------
    as_: scattering lengths
         default = (dimer-dimer, boson-boson, dimer-boson)
    """
    a11, a22, a12 = as_
    m1, m2 = ms
    gammas = np.array((4*np.pi*u.hbar**2*a11/m1,
                       4*np.pi*u.hbar**2*a22/m2))
    gamma_12 = 2*np.pi*u.hbar**2*a12*(m1+m2)/m1/m2
    return quad(rho_dr_integrand, 0, np.inf, args=(ms, ns, gammas, gamma_12))


def get_N_R(n0, B, m_f, w):
    """Return the particle number in a TF Ring trap

    Arguments:
    ----------
    B : External magnetic field
    w : trapping frequency
    m_f : Fermion mass
    n0 : central density
    N_R : Number of particles/Trap Radius
    """
    mu = eos_F.dE(n0, m_f=m_f, B=B) # chemical potential, first derivative of the fermionic EoS
    mu0 = eos_F.dE(0, m_f=m_f, B=B) # background chemical potential
    rho = np.sqrt(2*(mu-mu0)/m_f)/w

    def integrand(r):
        mu = mu0 + m_f*w**2/2 * (rho**2 - r**2)
        n = eos_F.get_n(mu, m_f=m_f, B=B)
        return 4*np.pi**2 * r*n

    return quad(integrand, 0, rho)[0]


def get_n0(N_R, B, m_f, w):
    """Return the central density of a TF Ring trap (Fermions).

    Arguments:
    ----------
    B : External magnetic field
    w : trapping frequency
    m_f : Fermion mass
    n0 : central density
    N_R : Number of particles/Trap Radius
    """
    n0 = 0
    n1 = 1.0
    def f(n):
        return get_N_R(n, B=B, m_f=m_f, w=w) - N_R
    while f(n1) < 0:
        n1 *= 2
    return brentq(f, n0, n1)

#_l = {}


def get_B_at_miscibility_bound(Nf_R, w_f,
                               m_f, gamma_bb, gamma_Db,
                               unitary_gamma=True):
    m_D = 2*m_f                    # Dimer mass

    def miscibility(B):
        ainv_ff = eos_F.get_ainv(B=B)  # Fermion-fermion scattering length
        n_f = get_n0(N_R=Nf_R, B=B, w=w_f, m_f=m_f) # fermion density at the center of a ring trap
        n_D = 0.5*n_f              # dimer density at the center of a ring trap

        # Relevant quantities for the fermi gas as a function of density
        kF = (3*np.pi**2 * n_f)**(1./3) # fermi vector
        eF = (u.hbar*kF)**2/2/m_f # fermi energy
        vF = u.hbar*kF/m_f # fermi velocity

        if unitary_gamma:
            # Use the phonon effective gamma from eos_F
            gamma_DD = 4*eos_F.ddE(n=n_f, B=B, m_f=m_f) # eos_F.ddE is the second derivative of the fermionic EoS
        else:
            a_DD = 0.6/ainv_ff              # Petrov's 3-body calculation
            gamma_DD = 4*np.pi*u.hbar**2*a_DD/m_D

        return gamma_Db**2 - gamma_DD*gamma_bb

    B = brentq(miscibility, 540*u.G, 870*u.G)
    return B


def get_gamma_Db_at_miscibility_bound(B, Nf_R, w_f,
                                      m_f, gamma_bb,
                                      unitary_gamma=True):
    m_D = 2*m_f                    # Dimer mass

    ainv_ff = eos_F.get_ainv(B=B)  # Fermion-fermion scattering length
    n_f = get_n0(N_R=Nf_R, B=B, w=w_f, m_f=m_f) # fermion density at the center of a ring trap
    n_D = 0.5*n_f              # dimer density at the center of a ring trap

    # Relevant quantities for the fermi gas as a function of density
    kF = (3*np.pi**2 * n_f)**(1./3) # fermi vector
    eF = (u.hbar*kF)**2/2/m_f # fermi energy
    vF = u.hbar*kF/m_f # fermi velocity

    if unitary_gamma:
        # Use the phonon effective gamma from eos_F
        gamma_DD = 4*eos_F.ddE(n=n_f, B=B, m_f=m_f) # eos_F.ddE is the second derivative of the fermionic EoS
    else:
        a_DD = 0.6/ainv_ff                     # Petrov's 3-body calculation
        gamma_DD = 4*np.pi*u.hbar**2*a_DD/m_D

    gamma_Db = np.sqrt(gamma_DD*gamma_bb)
    return gamma_Db


def get_phase_shift_percent(
        B_=638, R_=30, Nf=1e5, Nb=1e5,
        alpha_Li_Yb=6.2,
        freq_b_=400,
        P_=20.0*1e-6,
        v_vc=0.15,
        unitary_gamma=True,
        a_DD=None,
        a_bb=u.a_Yb_Yb,      # boson-boson scattering length
        a_Db=3.87*u.a_Li_Yb,     # dimer-boson scattering length
                                # mean-field result from Cui, PRA 90, 041603(R)
        m_b = u.m_Yb,
        m_f = u.m_Li,
        mb_mf=u.m_Yb/u.m_Li,
        mbmf=u.m_Li*u.m_Yb,
        bose_bose=False,
        hbar=u.hbar,
        fudge=1.0,
        bad_Bs_=[650, 720],  # Range of bad Bs because of high loss.
        check_miscibility=True,
        independent_mass=False,):
    
    """
    Returns:
    --------
    0: the kFa value for B_ value specified.
    1: the expected percentage phase shift value.
    2: the number of windings, set by critical velocity value.
    3: dimensionless B-field value (B_), either form miscibility or given input.
    4: density of Fermions in the ring.
    5: density of Bosons in the ring.
    6: trapping frequency for Fermions, set by relative polarizability.
    7: induced flow velocity in the Fermions.
    8: induced wave vector in the bosons.

    Arguments:
    ---------
    B_ : int
       External magnetic field (G).  Computed if None to saturate the
       miscibility bound.
    P_ : float
       Power of the trapping laser in Watts
    freq_b_ : float
       Trapping frequency for Bosons in Hz, converted to angular frequency
       w_b = 2*pi*freq_b_
    R_ : float
       Radius of ring (micron)
    Nf : int
       Number of fermions.
    Nb : int
       Number of bosons.
    alpha_Li_Yb : float
       Ratio of polarizability of Li over the polarizability of Yb.
    v_vc : float
        v_f/v_c, ratio of the flow velocity over the critical velocity.
    mb_mf : float
       Ratio m_b/m_f where m_f is the mass of fermionic species or of
    the first boson species if looking at a bose-bose mixture (see
    `bose_bose` and `a_dd`).
    mbmf : float
       Product m_b*m_f.
    a_DD : float, None
       Dimer-Dimer scattering length.  If None, then compute this from
       the fermionic equation of state (depends on B). If provided,
       then assume that the dimer is another boson.
    a_bb : float
        Boson-boson scattering length.
    a_Db : float
       Dimer-boson scattering length.
    bose_bose : bool
       If True, calculate the phase shift for a Yb-Yb mixture.
    unitary_gamma : bool
       If True, uses the effective interaction coefficient calculated
       from the fermionic EoS.
    fudge : float
       Fudge factor to suppress fermion phonon dispersion relationship
       to test the intuition that lowering the energy of the phonons
       will increase the entrainment effects.
    check_miscibility : bool
       If True, then assert that the system is miscible.
    independent_mass : bool
       If True, passes on the masses separately, instead of the product of
       the two or the ratio of the two form. Used for testing.
    """
    
    if independent_mass:
            m_b = m_b
            m_f = m_f
    else:
        m_b = np.sqrt(mbmf*mb_mf)
        m_f = mbmf/m_b

    R = R_ * u.micron
    w_b = 2.0*np.pi*freq_b_*u.Hz
    #P = P_ * u.watt
    #w_b = np.sqrt(P*u.alpha_Yb/m_b)   # Laser_Power = m_Li*w_f^2/alpha_Li = m_b*w_b^2/alpha_Yb

    #print(P/u.watt, w_b/u.Hz/2.0/np.pi)
    n_b = np.sqrt(Nb*m_b**2*w_b**2/8./np.pi**3/u.hbar**2/a_bb/R) #boson density at the center of a ring trap
    gamma_bb = 4*np.pi*hbar**2*a_bb/m_b
    #print(gamma_bb/(u.hbar*u.micron**3/u.s))
    m_D = 2*m_f                    # Dimer mass

    if bose_bose:
        assert a_dd is not None
        kFainv = None
        ms = (m_b, m_b)
        ns = (n_b, n_b)
        v_c = np.sqrt(gamma_bb*n_b/m_b)
        gammas = np.array([gamma_bb, gamma_bb])
        gamma_Db = 0.5*gamma_bb

    else:
        # Fermi-Bose mixture
        assert a_DD is None
        freq_factor = np.sqrt(m_b/m_f * alpha_Li_Yb)
        w_f = freq_factor*w_b # Angular trapping frequency for fermions
        gamma_Db = 2*np.pi*u.hbar**2*a_Db*(m_D+m_b)/m_D/m_b

        if B_ is None:
            B = get_B_at_miscibility_bound(
                Nf_R=Nf/R, w_f=w_f, m_f=m_f,
                gamma_bb=gamma_bb, gamma_Db=gamma_Db,
                unitary_gamma=unitary_gamma)
            if B > bad_Bs_[0]*u.G:
                B = min(B_*u.G, bad_Bs_[1]*u.G)
        else:
            B = B_*u.G

        ainv_ff = eos_F.get_ainv(B=B)  # Fermion-fermion scattering length

        # fermion density at the center of a ring trap
        n_f = get_n0(N_R=Nf/R, B=B, w=w_f, m_f=m_f)

        # dimer density at the center of a ring trap
        n_D = 0.5*n_f
        #print(n_b/u.micron**3, w_b/u.kHz, n_f/u.micron**3)
        # Relevant quantities for the fermi gas as a function of density
        kF = (3*np.pi**2 * n_f)**(1./3) # fermi vector
        eF = (u.hbar*kF)**2/2/m_f # fermi energy
        vF = u.hbar*kF/m_f # fermi velocity

        if unitary_gamma:
            # Use the phonon effective gamma from eos_F
            gamma_DD = 4*eos_F.ddE(n=n_f, B=B, m_f=m_f) # eos_F.ddE is the second derivative of the fermionic EoS
        else:
            a_DD = 0.6*1/ainv_ff                # Petrov's 3-body calculation
            gamma_DD = 4*np.pi*u.hbar**2*a_DD/m_D

        # Check miscibility
        if check_miscibility:
            #print("gamma_Db = {}".format(gamma_Db), "gamma_DD = {}".format(gamma_DD),
                 # "gamma_bb = {}".format(gamma_bb))
            #print(1+1e-12 - (gamma_Db**2/(gamma_DD*gamma_bb)))
            assert gamma_Db**2/(gamma_DD*gamma_bb) <= 1+1e-12

        v_c = np.sqrt(gamma_DD*n_D/m_D) # critical velocity

        gammas = np.array([gamma_DD*fudge, gamma_bb])
        ms = (m_D, m_b)
        ns = (n_D, n_b)

    # eq. (28) from Fill:2005
    def integrand(k):
        return rho_dr_integrand(k, ms=ms, ns=ns, gammas=gammas, gamma_12=gamma_Db)
    
    def integrand_simplified(k):
        return simplified_rho_dr_integrand(k, ms=ms, ns=ns, gammas=gammas, gamma_12=gamma_Db)

    # Update _l with locals so we can plot the integrand.
    #global _l
    #_l.update(locals())

    rho_ent, err = quad(integrand, 0, np.inf)
    rho_ent_simplified, err_simplified = quad(integrand_simplified, 0, np.inf)

    v_f = v_vc*v_c                  # flow velocity in the fermionic component
    # number of windings to create a flow in the first component.
    n_wind = m_D * v_f * R / u.hbar

    # Relevant quantities derived by minimizing the energy functional
    # with a flow in the first component, limited by the critical velocity.
    v_b = v_f / (1 + m_b * n_b / rho_ent) # response velocity
    k_b = m_b * v_b / u.hbar              # response wave vector
    dtheta = 2*np.pi * R * k_b            # Absolute phase shift
    phase_shift_percent = 100 * dtheta / (2*np.pi)
    PhaseShiftResults = namedtuple('PhaseShiftResults',
                                   ['kFainv', 'phase_shift_percent', 'n_wind', 'B_',
                                   'n_f', 'n_b', 'w_b', 'w_f', 'v_f', 'k_b', 'v_c', 'gamma_DD',
                                   'gamma_Db', 'gamma_bb', "mb_mf", "m_D","m_b", "rho_ent", "v_b", "mbmf",
                                   "ainv_ff","rho_ent_simplified"])
    if bose_bose:
        return PhaseShiftResults(kFainv=None,
                                 phase_shift_percent=phase_shift_percent)
    else:
        return PhaseShiftResults(kFainv=ainv_ff/kF,
                                 phase_shift_percent=phase_shift_percent,
                                 n_wind=n_wind, B_=B/u.G,
                                 n_f=n_f, n_b=n_b, w_b = w_b,
                                 w_f=w_f, v_f=v_f, k_b=k_b, v_c=v_c,
                                 gamma_DD=gamma_DD, gamma_Db=gamma_Db,
                                 gamma_bb=gamma_bb, mb_mf=mb_mf, m_D=ms[0],
                                 m_b=ms[1], rho_ent=rho_ent, v_b=v_b, mbmf=mbmf, 
                                 ainv_ff=ainv_ff, rho_ent_simplified=rho_ent_simplified)

def get_exponents():
    """Returns the exponents for calculating the phase shift
    from the scaling relationship in the paper.
    """
    exponents = dict(exp_B=-3.4, exp_R=0.46, exp_N_f=0.58, exp_N_b=-0.03,
                     exp_alpha=0.58, exp_a_Db0=2.02, exp_mb_mf=0.02, exp_mbmf=0.54,)
    return exponents

def get_scales():
    """Returns the quantities to set the scale for the scaling 
    relationship in the paper.
    """
    scales = dict(B0=638*u.G, a_Db0=3.87*u.a_Li_Yb, R0=30*u.micron,
                  N0=10**5, alpha0=6.2, v_vc0=0.15, factor=6.2e-2,)
    return scales

def scaling_law(B_=780, R_=30, Nf=1e5, Nb=1e5,
        alpha_Li_Yb=6.2,
        freq_b_=400,
        P_=20.0*1e-6,
        v_vc=0.15,
        unitary_gamma=True,
        a_DD=None,
        a_bb=u.a_Yb_Yb,      # boson-boson scattering length
        a_Db=3.87*u.a_Li_Yb, # dimer-boson scattering length
                             # mean-field result from Cui, PRA 90, 041603(R)
        mb_mf=u.m_Yb/u.m_Li,
        mbmf=u.m_Li*u.m_Yb,

        m_b = u.m_Yb,
        m_f = u.m_Li,

        bose_bose=False,
        hbar=u.hbar,
        fudge=1.0,
        bad_Bs_=[650, 720],  # Range of bad Bs because of high loss.
        check_miscibility=True,
        independent_mass=False,):
    """Returns the percentage phase shift calculated from the scaling
    relationship in the paper.
    
    Arguments:
    ----------
    Takes exactly same arguments as get_phase_shift_percent.
    """
    exponents = get_exponents()
    scales = get_scales()
    B = B_*u.G
    R = R_*u.micron
    
    a0 = scales["a_Db0"]
    R0 = scales["R0"]
    B0 = scales["B0"]
    N0 = scales["N0"]
    alpha0 = scales["alpha0"]
    v_vc0 = scales["v_vc0"]
    factor = scales["factor"]
    
    exp_B = exponents["exp_B"]
    exp_R = exponents["exp_R"]
    exp_N_f= exponents["exp_N_f"]
    exp_N_b= exponents["exp_N_b"]
    exp_alpha = exponents["exp_alpha"]
    exp_a_Db= exponents["exp_a_Db0"]
    exp_mb_mf= exponents["exp_mb_mf"]
    exp_mbmf= exponents["exp_mbmf"]
    
    A = ((v_vc/v_vc0)*(a_Db/a0)**exp_a_Db*(B/B0)**(exp_B)*(alpha_Li_Yb/alpha0)**(exp_alpha))
    B = ((R/R0)**(exp_R)*(Nf/N0)**(exp_N_f)*(Nb/N0)**(exp_N_b)*(mbmf/mbmf)**(exp_mbmf)*(mb_mf/mb_mf)**(exp_mb_mf))

    scaled_phase_shift = 100*factor*A*B
    return scaled_phase_shift

