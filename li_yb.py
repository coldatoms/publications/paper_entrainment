"""Coupled Li-Yb system.

Notes about the code
====================

The functionality is divided into two components: the Problem and the
State.  Here are their respective roles:

State
-----
Defines the abscissa and provides the appropriate functionality for
computing the kinetic energy etc.  The state specifies the number of
points, the box size, and the dimensionality of the system.  For
example, cylindrical coordinates vs cartesian coordinates are a
property of the State as is the resolution etc. of the basis.

The State class must provide the evolvers with an appropriate set of
methods required for time evolution as defined in
`pytimeode.interface`.  The State class has a `problem` that it uses
to compute model-dependent things such as applying the Hamiltonian
etc.

There is not much for a user to do with the State classes other than
to be aware that this is where the basis/box size is specified, and to
choose the appropriate State and basis for the problem.

Problem
-------
The problem class defines the numerical simulation.  This includes any
time-dependent parameters etc.  The Problem class works directly with
parameters of the underlying simulation class (defined in the
`coupled_bec.gpe2` and `coupled_bec.gpe1` modules).

Experiment
----------
We add an additional layer as an interface to the Problem that allows one to
specify experimental parameters in natural ..  The Experiment class should
define `get_time_dependent_parameters` which returns an instance of
`Experiment.TimeDependentParameters`.  This will be used by the Problems to
compute the appropriate simulation parameters.
"""

from __future__ import division, absolute_import, print_function

import numpy as np
from scipy.optimize import brentq
from scipy.interpolate import InterpolatedUnivariateSpline
import sympy

from mmfutils.containers import Object
from pytimeode.utils.expr import Expression

__all__ = ['units', 'EoSUFG']

_TINY = (np.finfo(float).tiny)**(1./16.)

from .units import u
units = u


class EoSUFG(Object):
    """Equation of state for a symmetric T=0 Fermi gas.

    Uses a single parameter Pade approximant to interpolate into the BEC
    limit.  This fits a large collection of QMC and perturbative data.

    Arguments
    ---------
    ainv : float, None
       Inverse scattering length. If None, then calculate from magnetic field.
    """

    def __init__(self, ainv=None, xi=0.3742, zeta=0.901, aDD_a=0.6,
                 hbar=u.hbar, b=0.25, use_numexpr=True):
        self.ainv = ainv

        n, m_f = sympy.S('n, m_f')
        S = sympy.S
        kF = (S(3) * sympy.pi**2 * n)**(S(1)/3)
        eF = (hbar*kF)**2/2/m_f
        EFG = S(3)/5 * n * eF
        ainv = S('ainv')
        self.zeta = zeta
        self.xi = xi
        self.aDD_a = aDD_a

        x = ainv/kF
        B = (b + self.zeta)/self.xi
        C = 18*np.pi*b/5./self.aDD_a

        self._EoS = ((self.xi + b*x - S(5)/3*x**2*(1 + B*x + C*x**2))
               / (1 + B*x + C*x**2)) * EFG
        self._dEoS = self._EoS.diff(n)
        self._ddEoS = self._EoS.diff(n, n)

        self.use_numexpr = use_numexpr

        if use_numexpr:
            args = ('n', 'ainv', 'm_f')
            eos_expr = Expression(str(self._EoS), args=args)
            deos_expr = Expression(str(self._dEoS), args=args)
            ddeos_expr = Expression(str(self._ddEoS), args=args)

            def _eos(n, ainv, m_f):
                res = np.empty_like(n)
                return eos_expr(res, n=n, ainv=ainv, m_f=m_f)

            def _deos(n, ainv, m_f):
                res = np.empty_like(n)
                return deos_expr(res, n=n, ainv=ainv, m_f=m_f)

            def _ddeos(n, ainv, m_f):
                res = np.empty_like(n)
                return ddeos_expr(res, n=n, ainv=ainv, m_f=m_f)

            self._eos = _eos
            self._deos = _deos
            self._ddeos = _ddeos
        else:
            self._eos = sympy.lambdify([n, ainv, m_f], self._EoS, np)
            self._deos = sympy.lambdify([n, ainv, m_f], self._dEoS, np)
            self._ddeos = sympy.lambdify([n, ainv, m_f], self._ddEoS, np)

        # These are for memoized spline approximations for n(mu)
        self._n_mu_splines = {}
        self._n_mu_spline_points = 100   # Number of points in n_mu splines

    def E(self, n, m_f, B=None):
        """Return the energy density as a function of density and magnetic
        field."""
        ainv = self.get_ainv(B=B)
        return self._eos(n+_TINY, ainv, m_f)

    def dE(self, n, m_f, B=None):
        """Return the derivative of the energy density as a function of density
        and magnetic field.
        """
        ainv = self.get_ainv(B=B)
        return self._deos(n+_TINY, ainv, m_f)

    # dE is also the chemical potential
    mu = dE

    def ddE(self, n, m_f, B=None):
        """Return the derivative of the energy density as a function of density
        and magnetic field.
        """
        ainv = self.get_ainv(B=B)
        return self._ddeos(n+_TINY, ainv, m_f)

    def n(self, mu, m_f, B):
        """Fast version of `get_n()` that uses a spline for large arrays."""
        mu_max = np.max(mu)
        splines = self._n_mu_splines
        if B not in splines or splines[B][1] <= mu_max:
            mu_min = self.dE(0, m_f=m_f, B=B)
            mus = np.linspace(mu_min, mu_max, self._n_mu_spline_points)
            ns = np.vectorize(self.get_n)(mus, m_f=m_f, B=B)
            splines[B] = (
                InterpolatedUnivariateSpline(mus, ns, ext=1),
                mu_max)
        return splines[B][0](mu)

    def get_n(self, mu, m_f, B=None):
        """Return the density as a function of mu = dE.

        Slow, but useful for TF solutions.
        """
        if mu <= self.dE(0, m_f=m_f, B=B):
            return 0.0

        def f(n):
            return self.dE(n, m_f=m_f, B=B) - mu

        # Use UFG as guess
        if mu <=0:
            # FIX
            n0 = n1 = 1.0
        else:
            kF = np.sqrt(2*m_f*mu/u.hbar**2/u.xi)
            n0 = n1 = kF**3/3/np.pi**2

        while f(n0) > 0:
            n0 /= 2.0
        while f(n1) < 0:
            n1 *= 2.0
        return brentq(f, n0, n1)

    def get_ainv(self, B=None, G=u.G, a_B=u.a_B):
        r"""Parameters come from fitting the data in [Zurn:2013] with the model
        give in PRL 94, 103201 (2005).  Their fits result from setting
        alpha=0."""
        if self.ainv is not None:
            return self.ainv

        B0 = 8.32178498e+02 * u.G
        if B is None:
            B = B0
        Delta = 2.93396620e+02 * u.G
        a_bg = -1.41505108e+03 * u.a_B
        alpha = 4.06405370e-04 / u.G
        dB = B-B0
        return dB/a_bg/(dB + Delta)/(1 + alpha*dB)
