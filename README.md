# Detecting Entraiment in Fermi-Bose Mixtures
This code repository provides scripts to calculate entrainment
phase shift for the parameters provided in our paper. 

Here we provide two notebooks:

1. PhaseshiftCodeDemonstration.ipynb: This shows how to use 
our code to calculate the entrainment phase shift.
2. SubmissionFigures.ipynb: One can use this notebook to generate
the figures in the paper using the data provided in 
[osf.io](https://osf.io/8kc2b/).

We also provide the following python files to perform the 
calculations.
1. phaseshift.py: Main code to do the calculation.
2. units.py: Contains the units that we have used.
3. li\_yb.py: Contains the equation of state for Li6.
4. figures.py: Contains the classes to draw the figures.
5. mpl\_styles.py and figure\_style.py: Style files for figure
	formatting.
6. anaconda-project.yaml: The yaml file to set up the environment
	with correct dependencies.
7. Makefile: May use this file to setup the project environment.

To set up the environment we need to run the following command:

**anaconda-project init**

For details, consult:
[anaconda-project](https://anaconda-project.readthedocs.io/en/latest/)

[Forbes-group-instructions](https://wsu-phys-581-fall-2021.readthedocs.io/en/latest/InstructorNotes.html)

Alternatively, one can do:

**make init**

This does a few extra things, which are not mandatory for running 
the code.

We need to pay attention to the relative imports in the notebook. Currently the
repository has the notebooks and python codes at the same level (i.e. main 
directory). This is done to keep the workflow simple. Although if the size of 
the project gets bigger and more organized approach is necessary, we may make 
sub-directory structure. If we want to move any of the notebooks to a 
sub-directory, the relative import statements should be changed so that main 
code and other auxiliary code files are imported from the importable module 
(i.e. the directory with __init__.py file, the main repository directory.) 
