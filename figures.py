"""Functions to draw figures for paper.
"""
from collections import namedtuple, OrderedDict
from functools import partial
import itertools

import numpy as np
import scipy as sp
from scipy.interpolate import InterpolatedUnivariateSpline, CubicSpline

from matplotlib import pyplot as plt
import matplotlib.path
import matplotlib.patches
from matplotlib.gridspec import GridSpec, GridSpecFromSubplotSpec
from matplotlib.ticker import ScalarFormatter

from mmfutils import plot as mmfplt
from mmfutils.plot import imcontourf

from figure_style import Paper, FigureSimple

#from paper_entrainment import phaseshift as ps
from phaseshift_code_submission import phaseshift as ps
from scipy.optimize import curve_fit
from phaseshift_code_submission.phaseshift import u, params_var, params_fixed, get_args
from phaseshift_code_submission.phaseshift import get_gamma_Db_at_miscibility_bound, get_B_at_miscibility_bound

from gpe.plot_utils import MPLGrid


class PaperFigures(Paper):
    #style = 'nature'
    figdir = "tmp_figdir/"
    style = 'arXiv'

    def fig_B_a_Db(self, constants=None):
        """Draw the B a_Db."""
        if constants:
            width = constants["width"]
            height = 1.1 * constants['golden_mean'] * width
            fig = plt.figure(figsize=(width, height))
            myfig = FigureSimple(num=1, fig=fig)
        else:
            myfig = self.figure(
                num=1,  # If you want to redraw in the same window
                width='columnwidth',  # For two-column documents, vs. 'textwidth'
                height=1.3,  # 1.3 is the default
                #margin_factors = dict(
                #    top=-2.9,
                #    left=3.9,
                #    bot=3.1,
                #    right=-3.4)
            )

        B_ = np.load("figdata/Bs_fig1.npy")
        ac = np.load("figdata/ac_fig_1.npy")
        af = np.load("figdata/af_fig_1.npy")

        ac_B_ = InterpolatedUnivariateSpline(ac, B_)
        af_B_ = InterpolatedUnivariateSpline(af, B_)

        ac_650 = np.load("figdata/ac_650.npy")
        af_650 = np.load("figdata/af_650.npy")
        ac_720 = np.load("figdata/ac_720.npy")
        af_720 = np.load("figdata/af_720.npy")

        inds_c = np.where((ac > ac_650) & (ac < ac_720))
        array_c = np.ma.masked_where((ac > ac_650) & (ac < ac_720), ac)

        inds_f = np.where((af > af_650) & (af < af_720))
        array_f = np.ma.masked_where((af > ac_650) & (af < af_720), af)

        plt.plot(ac / u.a_Li_Yb, B_, "tab:gray", linestyle="--", alpha=0.5)
        plt.plot(array_c / u.a_Li_Yb, B_, "tab:gray", label="current", lw=3.0)

        plt.plot(af / u.a_Li_Yb, B_, "tab:brown", linestyle="--", alpha=0.5)
        plt.plot(array_f / u.a_Li_Yb, B_, "tab:brown", label="future", lw=2.0)

        lx1 = np.linspace(4.2, 6.55, 100)
        ly1 = 721 * np.ones(100)
        plt.plot(lx1, ly1, "tab:gray", lw=3.0)

        lx2 = np.linspace(4.2, 4.2, 100)
        ly2 = np.linspace(650, 721, 100)
        plt.plot(lx2, ly2, "tab:gray", lw=3.0)

        lx3 = np.linspace(4.2, 6.55, 100)
        ly3 = 721 * np.ones(100)
        plt.plot(lx3, ly3, "tab:brown", lw=2.0)

        lx4 = np.linspace(4.2, 4.2, 100)
        ly4 = np.linspace(650, 721, 100)
        plt.plot(lx4, ly4, "tab:brown", lw=2.0)

        #plt.fill(B_, ac/u.a_Li_Yb, af/u.a_Li_Yb)

        plt.fill_between(ac / u.a_Li_Yb, B_, color="tab:grey", alpha=0.40)
        #plt.fill_between(ac/u.a_Li_Yb, B_, facecolor="none", hatch='+', edgecolor="gray")
        plt.ylim(540, 860)

        plt.axhspan(650, 720, facecolor='none', hatch="//", edgecolor="gray")
        #plt.axvline([2], ls=':')
        #plt.axvline([3.87], ls=':')
        plt.xlabel(r'$a_{Db}/a_{fb}$')
        plt.ylabel(r'$B$ [G]')
        #plt.plot(ac[inds]/u.a_Li_Yb, B_[inds], "g:")
        #plt.plot(ac[inds]/u.a_Li_Yb, B_[inds], "g:")
        plt.legend(loc="upper left")
        #plt.savefig('figures/B_a_Db_hatched_bw.pdf')
        #myfig.savefig(fname="tmp_figdir/B_a_Db.pdf")
        plt.tight_layout()
        return myfig

    def fig_log_ps_a_Db(self, constants=None):
        """Draw the B a_Db."""
        if constants:
            width = constants["width"]
            height = 1.3 * constants['golden_mean'] * width
            fig = plt.figure(figsize=(width, height))
            myfig = FigureSimple(num=3, fig=fig)
        else:
            myfig = self.figure(
                num=1,  # If you want to redraw in the same window
                width='columnwidth',  # For two-column documents, vs. 'textwidth'
                #height=1.3,
                #margin_factors = dict(
                #    top=-2.9,
                #    left=3.9,
                #    bot=3.1,
                #    right=-3.4)
            )

        B_ = np.load("figdata/Bs_fig1.npy")
        ac = np.load("figdata/ac_fig_1.npy")
        af = np.load("figdata/af_fig_1.npy")
        #B_, ac, af = list(map(np.asarray, get_data()))
        ac_B_ = InterpolatedUnivariateSpline(ac, B_)
        af_B_ = InterpolatedUnivariateSpline(af, B_)

        ps3_c_10 = np.load("figdata/ps_current_full.npy")
        ps3_f_10 = np.load("figdata/ps_future_full.npy")

        ps_fixB_current_200 = np.load("figdata/ps_fixB_current_200.npy")
        bad_as_fixB_current_200 = np.load(
            "figdata/bad_as_fixB_current_200.npy")
        a_Db_full_range_current_200 = np.load(
            "figdata/a_Db_full_range_current_200.npy")

        ps_fixB_future_200 = np.load("figdata/ps_fixB_future_200.npy")
        bad_as_fixB_future_200 = np.load("figdata/bad_as_fixB_future_200.npy")
        a_Db_full_range_future_200 = np.load(
            "figdata/a_Db_full_range_future_200.npy")

        ac_650 = np.load("figdata/ac_650.npy")
        af_650 = np.load("figdata/af_650.npy")
        ac_720 = np.load("figdata/ac_720.npy")
        af_720 = np.load("figdata/af_720.npy")

        _ac_650 = ac_650 / u.a_Li_Yb
        _af_650 = af_650 / u.a_Li_Yb
        _ac_720 = ac_720 / u.a_Li_Yb
        _af_720 = af_720 / u.a_Li_Yb

        cs = CubicSpline(a_Db_full_range_current_200 / u.a_Li_Yb,
                         ps_fixB_current_200)
        _xs = np.linspace(1.2, 9.0, 200)

        cs_future = CubicSpline(a_Db_full_range_future_200 / u.a_Li_Yb,
                                ps_fixB_future_200)
        _xs_future = np.linspace(1.2, 9.0, 200)

        _arr_xs = np.where((_xs >= (_ac_650)) & (_xs <= _ac_720))
        #plt.semilogy(_xs, cs(_xs))
        #print(_arr_xs, _xs[_arr_xs], cs(_xs)[_arr_xs])

        _arr_xs_f = np.where((_xs_future >= _af_650) & (_xs_future <= _af_720))
        #plt.semilogy(_xs, cs(_xs))
        #print(_xs[_arr_xs], cs(_xs)[_arr_xs], _xs_future[_arr_xs_f], cs_future(_xs_future)[_arr_xs_f])

        ps3_c_10_modified = ps3_c_10.copy()
        ps3_c_10_modified[(_arr_xs[0][0] -
                           1):(_arr_xs[0][-1])] = cs(_xs)[_arr_xs]
        plt.semilogy(ac / u.a_Li_Yb,
                     ps3_c_10_modified,
                     'tab:grey',
                     lw=3.0,
                     label="current")
        plt.semilogy(ac / u.a_Li_Yb,
                     ps3_c_10,
                     "tab:grey",
                     linestyle="--",
                     alpha=0.5)

        ps3_f_10_modified = ps3_f_10.copy()
        ps3_f_10_modified[(_arr_xs_f[0][0] - 1):(
            _arr_xs_f[0][-1])] = cs_future(_xs_future)[_arr_xs_f]
        plt.semilogy(af / u.a_Li_Yb,
                     ps3_f_10_modified,
                     "tab:brown",
                     lw=2.0,
                     label="future")
        plt.semilogy(af / u.a_Li_Yb,
                     ps3_f_10,
                     "tab:brown",
                     linestyle="--",
                     alpha=0.5)

        plt.xlabel(r'$a_{Db}/a_{fb}$')
        plt.ylabel(r'phase shift$(\%)$')

        ps_ticks = [1, 5, 10, 50, 100]
        ax = plt.gca()
        ax.set_yscale('log')
        ax.yaxis.set_major_formatter(ScalarFormatter())
        ax.yaxis.set_ticks(ticks=ps_ticks)
        plt.legend()

        plt.text(4.5, 2.5, "B=730G", rotation=20)
        plt.text(4.5, 28.5, "B=730G", rotation=25)

        plt.text(1.8, 1.5, "Miscible B", rotation=20)
        plt.text(1.8, 14.5, "Miscible B", rotation=20)

        plt.text(7.0, 5.5, "Miscible B", rotation=10)
        plt.text(6.8, 70.5, "Miscible B", rotation=5)

        plt.tight_layout()
        return myfig

    def fig_ps_B(self, constants=None):
        """Draw the B a_Db."""
        if constants:
            width = constants["width"]
            height = 1.3 * constants['golden_mean'] * width
            fig = plt.figure(figsize=(width, height))
            myfig = FigureSimple(num=2, fig=fig)
        else:
            myfig = self.figure(
                num=1,  # If you want to redraw in the same window
                width='columnwidth',  # For two-column documents, vs. 'textwidth'
                #height=1.3,
                #margin_factors = dict(
                #    top=-2.9,
                #    left=3.9,
                #    bot=3.1,
                #    right=-3.4)
            )

        res_c = np.load("figdata/res_c_fig3.npy")
        res_f = np.load("figdata/res_f_fig3.npy")
        Bs = np.load("figdata/Bs_fig3.npy")

        kFas = np.load("figdata/kFas_fig3.npy")
        kFas_future = np.load("figdata/kFas_future_fig3.npy")
        phase_shift_percents = np.load("figdata/phase_shift_percents_fig3.npy")
        phase_shift_percents_future = np.load(
            "figdata/phase_shift_percents_future_fig3.npy")

        inds = np.argsort(1. / kFas)
        B_kFainv = sp.interpolate.UnivariateSpline(1. / kFas[inds], Bs[inds])

        inds_f = np.argsort(1. / kFas_future)
        B_kFainv_future = sp.interpolate.UnivariateSpline(
            1. / kFas_future[inds_f], Bs[inds_f])

        xlim = (600, 850)
        plt.semilogy(
            Bs,
            phase_shift_percents,
            "tab:grey",
            lw=3.0,
            label="current",
        )
        plt.semilogy(
            Bs,
            phase_shift_percents_future,
            "tab:brown",
            lw=2.0,
            label="future",
        )
        plt.axvspan(
            650,
            720,
            facecolor='none',
            hatch="//",
            lw=1.0,
            edgecolor="gray",
        )
        plt.xlabel('B(G)')
        plt.ylabel(r'phase shift($\%$)')
        plt.xlim(xlim)
        plt.legend()
        #plt.set_axisbelow(True)

        ps_ticks = [1, 5, 10, 40, 60]
        ax2 = plt.gca()
        ax2.set_yscale('log')
        ax2.yaxis.set_major_formatter(ScalarFormatter())
        ax2.yaxis.set_ticks(ticks=ps_ticks)
        #ax2.xaxis.grid()
        #ax2.set_axisbelow(True)

        ax1 = plt.twiny()
        kFainv_ticks = [-2, -1, 0, 1, 2, 3, 4]
        xticks = B_kFainv(kFainv_ticks)
        xticklabels = list(map(str, kFainv_ticks))
        ax1.set_xticks(xticks)
        ax1.set_xticklabels(xticklabels)
        ax1.xaxis.grid()
        ax1.set_axisbelow(True)
        plt.xlim(xlim)
        plt.xlabel('$1/(k_F a)$')

        #plt.figure(axisbelow=True)

        #fig = plt.gcf()
        #ax0 = fig.axes[-1]
        #ax0.set_axisbelow(True)

        #plt.text(750, 1.2, 'Unitarity (832G)')
        #plt.text(610, 1.2, 'BEC-side')

        plt.tight_layout()
        return myfig

    def fig_expansion(self, constants):
        """Draw the B a_Db."""
        width = constants["width"]
        height = 1.1 * constants['golden_mean'] * width
        print(height)
        fig, axs = plt.subplots(6,
                                2,
                                constrained_layout=True,
                                sharex=True,
                                sharey=False,
                                figsize=(width, height))
        fig.set_constrained_layout_pads(h_pad=0.01,
                                        w_pad=0,
                                        hspace=0,
                                        wspace=0.03)

        myfig = FigureSimple(num=4, fig=fig)

        ts_ = [1, 2, 10, 20, 30, 60]

        x_n_ts = []
        x_n_ts_f = []
        x_n_ts_no = []
        x_n_ts_f_no = []
        for t_ in ts_:
            s1d = np.load(f"figdata/tube_state_{t_}ms_current_trial_paper.npz")
            s1d_f = np.load(
                f"figdata/tube_state_{t_}ms_future_trial_paper.npz")
            s1d_no = np.load(
                f"figdata/tube_state_{t_}ms_no_current_trial_paper.npz")
            s1d_f_no = np.load(
                f"figdata/tube_state_{t_}ms_no_future_trial_paper.npz")
            x = s1d["xs"]
            x_f = s1d_f["xs"]
            x_no = s1d_no["xs"]
            x_f_no = s1d_f_no["xs"]
            n = s1d["state_density"]
            n_f = s1d_f["state_density"]
            n_no = s1d_no["state_density"]
            n_f_no = s1d_f_no["state_density"]
            t = s1d["t"]
            t_f = s1d_f["t"]
            t_no = s1d_no["t"]
            t_f_no = s1d_f_no["t"]
            x_n_ts.append((x, n, t))
            x_n_ts_f.append((x_f, n_f, t_f))
            x_n_ts_no.append((x_no, n_no, t_no))
            x_n_ts_f_no.append((x_f_no, n_f_no, t_f_no))

        for row, res in enumerate(zip(x_n_ts, x_n_ts_f, x_n_ts_no,
                                      x_n_ts_f_no)):
            (x, n, t), (x_f, n_f, t_f), (x_no, n_no, t_no), (x_f_no, n_f_no,
                                                             t_f_no) = res

            axL, axR = axs[row, :]

            axR.yaxis.tick_right()

            for ax, c, label, (x_, n_, x_no_, n_no_) in zip(
                (axL, axR), ('C1', 'C0'), ('Current', 'Future'),
                ((x, n, x_no, n_no), (x_f, n_f, x_f_no, n_f_no))):
                #axs.append(ax)
                ax.plot(x_, n_, '-', c=c, label=fr"$\text{{{label}}}$")
                ax.plot(
                    x_no_,
                    n_no_,
                    ':',
                    c=c,
                    lw=0.8,
                    alpha=0.8,
                )
                ax.axvline(0.0, ls=':', c='k')
                ax.text(0.02,
                        0.05,
                        f"$t=\SI{{{t/u.ms:.1f}}}{{ms}}$",
                        transform=ax.transAxes,
                        fontsize='small',
                        verticalalignment="bottom",
                        bbox=dict(facecolor='white',
                                  alpha=0.7,
                                  boxstyle='round,pad=0.1',
                                  edgecolor='none'))
                ax.set(xlim=(-20, 20), ylim=(0, ax.get_ylim()[1]))

        for _ax in axs[:-1, :].ravel():
            plt.setp(_ax.xaxis.get_ticklabels(), visible=False)

        axs[-1, 0].set(xlabel="$L_x$ [\si{\micro m}]",
                       ylabel="$n_b$ [\si{\micro m^{-3}}]")

        axs[-1, 1].set(xlabel="$L_x$ [\si{\micro m}]",
                       ylabel="$n_b$ [\si{\micro m^{-3}}]")

        axs[-1, 0].axvspan(2.1240234375, 2.63671875, alpha=0.3)
        axs[-1, 1].axvspan(2.734375, 7.12890625, alpha=0.3)
        axs[0, 0].legend(loc='lower right')
        axs[0, 1].legend(loc='lower right')

        return myfig

    def fig_radial(self, constants):
        """Draw the B a_Db."""
        width = constants["width"]
        height = 0.36 * width
        print(height)
        fig = plt.figure(figsize=(width, height))
        fig, axs = plt.subplots(1,
                                5,
                                constrained_layout=True,
                                sharex=True,
                                sharey=True,
                                figsize=(width, height))
        fig.set_constrained_layout_pads(h_pad=0.05, w_pad=0.01, wspace=0.01)
        myfig = FigureSimple(num=5, fig=fig)

        ts_ = [0, 1, 2, 5, 10]

        x_n_ts = []
        for t_ in ts_:
            s2d = np.load(f"figdata/state_{t_}_rad.npz")
            x = s2d["xs"]
            y = s2d["ys"]
            n = abs(s2d["state_data"][:, :, -1])**2
            t = s2d["t"]
            x_n_ts.append((x, n, t))

        #myfig.ax.remove()

        #grid = MPLGrid(fig=myfig.fig,
        #subplot_spec=myfig.subplot_spec,
        #direction='right',
        #share=True,
        #space=0.05)  # default 0.05

        #axs = []
        n_max = x_n_ts[0][1].max()
        for ax, (x, n, t) in zip(axs, x_n_ts):
            #subgrid = grid.grid(direction='right', space=0.1)

            #ax = subgrid.next()

            #axs.append(ax)
            plt.sca(ax)
            imcontourf(x, y, n, vmin=0, vmax=n_max, aspect=1)
            ax.grid(False)
            #ax.set_xticks([-40, -20, 0, 20, 40])
            ax.set(xlim=(-30, 30))
            #ax.axvline(0.0, ls=':', c='k')
            plt.title(f"$t=\SI{{{t/u.ms:.1f}}}{{ms}}$")
            #ax.text(-40, -35,
            # f"$t=\SI{{{t/u.ms:.1f}}}{{ms}}$",
            # fontsize='small',
            # verticalalignment="bottom",
            # bbox=dict(facecolor='white', alpha=0.7,
            # boxstyle='round,pad=0.1',
            # edgecolor='none')
            #)

        for _ax in axs[1:]:
            plt.setp(_ax.yaxis.get_ticklabels(), visible=False)
            #plt.setp(_ax.xaxis.get_ticklabels(), visible=False)

        axs[0].set(xlabel=r"$L_x$ [$\si{\micro m}$]",
                   ylabel=r"$L_y$ [$\si{\micro m}$]")
        plt.sca(axs[-1])
        plt.colorbar(fraction=0.46, pad=0.04, label=r"$n$ [$\si{\micro m^{-3}}$]")
        plt.setp(axs[0].yaxis.get_ticklabels(), visible=True)

        return myfig

    def fig_ps_m_Li(self):
        """Draw the B a_Db."""
        myfig = self.figure(
            num=1,  # If you want to redraw in the same window
            width='columnwidth',  # For two-column documents, vs. 'textwidth'
            height=1.3,  # 1.3 is the default
            #margin_factors = dict(
            #    top=-2.9,
            #    left=3.9,
            #    bot=3.1,
            #    right=-3.4)
        )

        ms_f_load = np.load("figdata/ms_f.npy")
        res_m_f_current_load = np.load("figdata/res_m_f_current.npy")
        res_m_f_future_load = np.load("figdata/res_m_f_future.npy")

        plt.plot(ms_f_load / u.m_Li,
                 res_m_f_current_load,
                 "tab:grey",
                 lw=3.0,
                 label="current")
        plt.plot(ms_f_load / u.m_Li,
                 res_m_f_future_load,
                 "tab:brown",
                 lw=2.0,
                 label="future")

        ps_ticks = [1, 5, 10, 20, 30, 40]
        ax = plt.gca()
        ax.set_yscale('log')
        ax.yaxis.set_major_formatter(ScalarFormatter())
        ax.yaxis.set_ticks(ticks=ps_ticks)

        plt.xlabel("$m_f/m_{Li}$")
        plt.ylabel("phase shift $(\%)$")
        plt.legend()

        return

    def fig_log_ps_a_Db_200(self):
        """Draw the B a_Db."""
        myfig = self.figure(
            num=1,  # If you want to redraw in the same window
            width='columnwidth',  # For two-column documents, vs. 'textwidth'
            #height=1.3,
            #margin_factors = dict(
            #    top=-2.9,
            #    left=3.9,
            #    bot=3.1,
            #    right=-3.4)
        )
        B_ = np.load("figdata/Bs_fig1.npy")
        ac = np.load("figdata/ac_fig_1.npy")
        af = np.load("figdata/af_fig_1.npy")

        ac_B_ = InterpolatedUnivariateSpline(ac, B_)
        af_B_ = InterpolatedUnivariateSpline(af, B_)

        ps3_c_10 = np.load("figdata/ps_current_full.npy")
        ps3_f_10 = np.load("figdata/ps_future_full.npy")

        ps_fixB_current_200 = np.load("figdata/ps_fixB_current_200.npy")
        bad_as_fixB_current_200 = np.load(
            "figdata/bad_as_fixB_current_200.npy")
        a_Db_full_range_current_200 = np.load(
            "figdata/a_Db_full_range_current_200.npy")

        ps_fixB_future_200 = np.load("figdata/ps_fixB_future_200.npy")
        bad_as_fixB_future_200 = np.load("figdata/bad_as_fixB_future_200.npy")
        a_Db_full_range_future_200 = np.load(
            "figdata/a_Db_full_range_future_200.npy")

        _ac_650 = 4.27030875 * u.a_Li_Yb
        _af_650 = 4.2679429 * u.a_Li_Yb
        _ac_720 = 6.56717474 * u.a_Li_Yb
        _af_720 = 6.48074564 * u.a_Li_Yb

        cs = CubicSpline(a_Db_full_range_current_200 / u.a_Li_Yb,
                         ps_fixB_current_200)
        _xs = np.linspace(1.2, 9.0, 200)

        cs_future = CubicSpline(a_Db_full_range_future_200 / u.a_Li_Yb,
                                ps_fixB_future_200)
        _xs_future = np.linspace(1.2, 9.0, 200)

        _arr_xs = np.where((_xs >= (_ac_650) / u.a_Li_Yb)
                           & (_xs <= _ac_720 / u.a_Li_Yb))

        _arr_xs_f = np.where((_xs_future >= _af_650 / u.a_Li_Yb)
                             & (_xs_future <= _af_720 / u.a_Li_Yb))

        ps3_c_10_modified = ps3_c_10.copy()
        ps3_c_10_modified[(_arr_xs[0][0] -
                           1):(_arr_xs[0][-1])] = cs(_xs)[_arr_xs]
        plt.semilogy(ac / u.a_Li_Yb,
                     ps3_c_10_modified,
                     'tab:grey',
                     lw=3.0,
                     label="current")
        plt.semilogy(ac / u.a_Li_Yb,
                     ps3_c_10,
                     "tab:grey",
                     linestyle="--",
                     alpha=0.5)
        plt.plot(3.87, 6.2, 'kx', alpha=0.5)
        plt.plot(2, 2.2862924828547593, 'k+', alpha=0.5)

        ps3_f_10_modified = ps3_f_10.copy()
        ps3_f_10_modified[(_arr_xs_f[0][0] - 1):(
            _arr_xs_f[0][-1])] = cs_future(_xs_future)[_arr_xs_f]
        plt.semilogy(af / u.a_Li_Yb,
                     ps3_f_10_modified,
                     "tab:brown",
                     lw=2.0,
                     label="future")
        plt.semilogy(af / u.a_Li_Yb,
                     ps3_f_10,
                     "tab:brown",
                     linestyle="--",
                     alpha=0.5)
        plt.plot(3.87, 70.19859936007656, 'kx', alpha=0.5)
        plt.plot(2, 25.886214519979987, 'k+', alpha=0.5)

        plt.xlabel(r'$a_{Db}/a_{fb}$')
        plt.ylabel(r'phase shift$(\%)$')

        ps_ticks = [1, 5, 10, 50, 100]
        ax = plt.gca()
        ax.set_yscale('log')
        ax.yaxis.set_major_formatter(ScalarFormatter())
        ax.yaxis.set_ticks(ticks=ps_ticks)
        plt.legend()

        plt.text(4.5, 2.5, "B=730G", rotation=20)
        plt.text(4.5, 28.5, "B=730G", rotation=25)

        plt.text(1.8, 1.2, "Miscible B", rotation=20)
        plt.text(1.8, 14.5, "Miscible B", rotation=20)

        plt.text(7.0, 5.5, "Miscible B", rotation=10)
        plt.text(6.8, 70.5, "Miscible B", rotation=5)

        #plt.savefig("figures/log_ps_a_Db_fix_B_future_current_kinks.pdf")

    def fig_density(self):
        """Draw the figure showing density drop and fringe
        width increase.
        """
        myfig = self.figure(
            num=1,  # If you want to redraw in the same window
            width='columnwidth',  # For two-column documents, vs. 'textwidth'
            height=1.3,  # 1.3 is the default
            #margin_factors = dict(
            #    top=-2.9,
            #    left=3.9,
            #    bot=3.1,
            #    right=-3.4)
        )

        s_no_ent_8ms_load = np.load("figdata/state_no_ent_8ms.npz")
        s_no_ent_no_expand_8ms_load = np.load(
            "figdata/state_no_ent_no_expand_8ms.npz")
        t = s_no_ent_8ms_load["t"]

        plt.plot(s_no_ent_8ms_load['xs'], s_no_ent_8ms_load["n"], "tab:brown")
        plt.plot(s_no_ent_no_expand_8ms_load['xs'],
                 s_no_ent_no_expand_8ms_load["n"], "tab:grey")
        plt.xlim(-20, 20)
        plt.xlabel("$L_x$ [\si{\micro m}]")
        plt.ylabel("$n_B$ [\si{\micro m^{-3}}]")
        plt.text(-18,
                 3,
                 f"$t=\SI{{{t/u.ms:.1f}}}{{ms}}$",
                 fontsize='small',
                 verticalalignment="bottom",
                 bbox=dict(facecolor='white',
                           alpha=0.7,
                           boxstyle='round,pad=0.1',
                           edgecolor='none'))
        return


'''
    def fig_DSW_1D(self, **kw):
        return self.fig_DSW(tube=False, axial=False, **kw)

    def fig_DSW_tube(self, **kw):
        return self.fig_DSW(tube=True, axial=False, **kw)

    def fig_DSW_axial(self, **kw):
        return self.fig_DSW(tube=True, axial=True, **kw)

    def fig_phonon_dispersion(self):
        """Draw the phonon dispersion."""

        fig = self.figure(
            num=1,                # If you want to redraw in the same window
            width='columnwidth',  # For two-column documents, vs. 'textwidth'
            height=1.0,
        )

        e = self.get_experiment()
        s = e.get_state()

        m = s.ms.mean()

        # Plot with phonon dispersion for several Omegas.
        k_R = e.k_r
        E_R = e.E_R
        ks_ = np.linspace(-2, 1.5, 100)
        ks = ks_ * k_R
        qs = u.hbar * ks

        E_ = e.get_dispersion()
        k0 = E_.get_k0()
        E0_ = E_(k0)[0]
        p0 = u.hbar * k0 * k_R
        g = gmean(e.gs)
        n = e.fiducial_V_TF/g

        phonon = PhononDispersion(
            gs=e.gs, ms=e.ms, hbar=u.hbar, k_R=k_R,
            Omega=e.Omega, delta=e.delta)

        @np.vectorize
        def Eph(q, _Eph=phonon.get_phonon_dispersion(p=p0, n=n)):
            """Return the lowest phonon branch (vectorized)."""
            Es_ = _Eph(q)
            return min(Es_[np.where(Es_ > 0)])

        @np.vectorize
        def get_na_n(q):
            mu, ns = phonon.get_homogeneous_state(p=q, n=n)
            return ns[0]/n

        Es_phonon = Eph(qs-p0)
        Es_particle = (E_(ks_)[0] - E0_)*2*E_R

        x, y = ks_, Es_particle/2/E_R

        color_according_to_species = False

        ax = plt.gca()

        if color_according_to_species:
            na_n = get_na_n(qs)
            nb_n = 1 - na_n
            ca = np.array([0, 0, 1, 1])[None, :]
            cb = np.array([1, 0, 0, 1])[None, :]
            colors = na_n[:, None]*ca + nb_n[:, None]*cb

            # https://matplotlib.org/3.1.1/gallery/lines_bars_and_markers/
            #         multicolored_line.html
            from matplotlib.collections import LineCollection
            points = np.array([x, y]).T.reshape(-1, 1, 2)
            segments = np.concatenate([points[:-1], points[1:]], axis=1)
            lc = LineCollection(segments, colors=colors, lw=3)
            #lc.set_array(na_n-nb_n)
            line = ax.add_collection(lc)
            #plt.colorbar(line)
            c0 = 'k'
        else:
            l, = plt.plot(x, y, c='C0', alpha=0.5, lw=2,
                          ls='--', label='Single Particle')
            c0 = 'k'

        ix = np.where(E_(ks_, d=2)[0] < 0)[0]
        plt.fill_betweenx(x, x[ix[0]], x[ix[-1]],
                          alpha=0.3)

        plt.plot(ks_, Es_phonon/2/E_R, label="Phonon",
                 c=c0, ls='-')

        plt.title(r'$\Omega={:.2f}E_R$, $\delta={:.2f}E_R$'.format(
            e.Omega/e.E_R, e.delta/e.E_R),
                  fontsize='small')

        plt.xlim(-2, 1.5)
        plt.xticks([-2,-1,0,1])
        plt.ylim(0, 0.7)
        plt.xlabel(r'$q/\hbar k_R$')
        plt.ylabel(r'Energy ($2E_R$)')
        plt.legend()
        #plt.title("Phonon Dispersion")
        #plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        return fig

    def fig_stages(self, data=None, stream=True,
                   y_tilt=0.01, column=False):
        """Figure showing the stages of evolution of the DSW.

        Arguments
        ---------
        data : None, dict
           Pre-computed data can be passed in.  Get this from
           self._data_stages.
        column : bool
           If True, then typeset the figure as a single column,
           otherwise go for full width.

        The grid is constructed as a 4x4 super-grid each with a 2x1
        grid of density on top and contours below.
        """
        t_s = [1.0, 5.0, 12.0, 10+14.0]
        t_s = [1.0, 5.0, 12.0, 10+10.0]

        xlim1 = (-12, 12)
        xlim2 = (-38, 38)
        ylim = (-3, 3)

        # Margins and padding - relative to full size
        left = 0.06
        right = 0.06
        wspace = 0.02

        top = 0.05
        bottom = 0.06
        hspace = 0.1

        # Height and width of various frames.
        w1, w2 = np.diff(xlim1)[0], np.diff(xlim2)[0]
        w3 = 0.1*w1
        h2 = h3 = np.diff(ylim)[0]
        h1 = 1*h2
        width =  w1 + w2 + w3
        height = 2*(h1 + h2 + h3)

        fig_width = width/(1 - left - right - wspace)
        fig_height = height/(1 - top - bottom - hspace)
        fig_aspect = fig_height/fig_width

        myfig = self.figure(
            num=3,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=f"{fig_aspect}*width",
            #fig_box=0.1,        # Shows outline for placement of text
            constrained_layout=False,  # We manage our own margins
            subplot_args=dict(left=left, bottom=bottom,
                              right=1.0-right, top=1.0-top,
                              hspace=hspace, wspace=wspace),
        )

        if data is None:
            import runs_car_fast_final
            #run = runs_car_fast_final.Run_IPG_small_xTF150()

            # Axial data
            run = runs_car_fast_final.Run_IPG_xTF150()
            sim = run.simulations[1]
            print(f"Loading data from {sim._dir_name}")
            states = [sim.get_state(t_=t_, image=False)
                      for t_ in t_s]

            # 3D data
            run = runs_car_fast_final.Run_IPG_small_3D_big()
            attrs = ['barrier_depth_nK']
            sims = [_sim for _sim in run.simulations
                    if (_sim.experiment.y_tilt == y_tilt
                        and all(
                            getattr(sim.experiment, _attr) ==
                            getattr(_sim.experiment, _attr)
                            for _attr in attrs))]
            assert len(sims) == 1
            sim3D = sims[0]
            print(f"Loading data from {sim._dir_name}")
            states3D = [sim3D.get_state(t_=t_, image=False)
                        for t_ in t_s]

            # Store for future use.
            data = dict(states=states, sim=sim,
                        states3D=states3D, sim3D=sim3D)


        states = list(zip(data['states'], data['states3D']))
        sim, sim3D = data['sim'], data['sim3D']
        self._data_stages = data

        myfig.ax.remove()       # Don't use base axis.
        # The right-most frame is the colorbar.
        gs0 = GridSpec(2, 3,
                       figure=myfig.fig,
                       height_ratios=[h1+h2+h3]*2,
                       width_ratios=[w1, w2, w3],
                       left=left, right=1-right,
                       bottom=bottom, top=1-top,
                       hspace=hspace,
                       wspace=wspace)
        gs0s =[gs0[0, 0], gs0[1, 0], gs0[0, 1], gs0[1, 1]]

        # Colorbar axis
        cax = plt.subplot(gs0[:, 2])
        #ax0 = (grid1.next(h1), grid1.next(h2))
        #ax1 = (grid1.next(h1), grid1.next(h2))
        #ax2 = (grid2.next(h1), grid2.next(h2))
        #ax3 = (grid2.next(h1), grid2.next(h2))

        ax0s = []
        for gs in gs0s:
            # Steal a little space between the figures without mucking
            # up the perfect alignment of the lowest frame which has
            # fixed aspect ratio.
            gs1 = GridSpecFromSubplotSpec(
                5, 1, gs,
                height_ratios=(0.8*h1, 0.1*h1, h2, 0.1*h1, h3),
                hspace=0.0, wspace=0.0)
            ax0s.append((plt.subplot(gs1[0, 0]),
                         plt.subplot(gs1[2, 0]),
                         plt.subplot(gs1[-1, 0])))

        n1_max = max(s.get_density_x(mean=False).sum(axis=0).max()
                     for (s, s3) in states)
        n_max = max(s.get_density().sum(axis=0).max() for (s, s3) in states)
        xlims = [xlim1, xlim1, xlim2, xlim2]
        for axs, xlim, (s, s3) in zip(ax0s, xlims, states):
            n1_unit = 1./u.nm
            n_unit = 100./u.micron**3
            x_unit = u.micron

            ax_a, ax_b, ax_c = axs
            psi = psi_a, psi_b = s.get_psi()
            n1_a, n1_b = s.get_density_x(mean=False)
            n1 = n1_a + n1_b
            n_a, n_b = s.get_density()
            n0 = n_a + n_b
            x, r0 = s.xyz

            # Compute the axial velocities for the streamline plot.
            # We first deal with the x-direction
            kx = s.basis.kx
            px = (
                psi.conj()*s.basis.ifft(s.hbar*kx*s.basis.fft(psi))
            ).sum(axis=0).real #/ n0
            pr = (-1j*s.hbar*(
                psi_a.conj()*np.gradient(
                    psi_a, x.ravel(), r0.ravel())[1]
                +
                psi_b.conj()*np.gradient(
                    psi_b, x.ravel(), r0.ravel())[1])
            ).real #/ n0

            r = np.hstack([-r0[:,::-1], r0])
            x_, r_ = x.ravel(), r.ravel()

            psi = np.hstack([psi_a[:,::-1], psi_a])
            n = np.hstack([n0[:,::-1], n0])
            px = np.hstack([px[:,::-1], px])
            pr = np.hstack([-pr[:,::-1], pr])

            r_new = np.linspace(0.9*ylim[0], 0.9*ylim[1],
                                len(r.ravel()))[1:-1][None, :]
            px = sp.interpolate.RegularGridInterpolator(
                (x.ravel(), r.ravel()), px)((x, r_new))
            pr = sp.interpolate.RegularGridInterpolator(
                (x.ravel(), r.ravel()), pr)((x, r_new))

            # Plot 1D integrated densities
            n1 = sum(s.get_density_x(mean=False))
            n13 = sum(s3.get_density_x(mean=False))
            x = s.xyz[0].ravel()
            x3 = s3.xyz[0].ravel()

            sim_axial_color = '#377eb8'
            sim_3d_color = '#ff7f00'
            ax = ax_a
            plt.sca(ax)
            plt.plot(x/x_unit, n1/n1_unit, c=sim_axial_color, ls='--',
                     alpha=0.8,
                     label='Axial')
            plt.plot(x3/x_unit, n13/n1_unit, c=sim_3d_color, ls='-',
                     alpha=0.8,
                     label='3D')
            plt.ylim(-0.1*n1_max/n1_unit, n1_max*1.1/n1_unit)
            ax.set_yticks([0, 2])

            # Add phantom + so that the label aligns with lower plot.
            ax.set_yticklabels([r"$\phantom{+}0$", r"$\phantom{+}2$"])

            # Draw 3D axial contours
            ax = ax_c
            plt.xlim(xlim)
            plt.sca(ax)
            #z = mmfplt.colors.color_complex(psi)
            x3, y3 = s3.xyz[:2]
            x3, y3 = x3[..., 0], y3[..., 0]
            n3 = sum(s3.get_density())[:, :, s3.shape[2]//2]
            mmfplt.imcontourf(x3, y3, n3/n_unit, vmin=0, vmax=n_max/n_unit,
                              aspect=1)
            plt.colorbar(cax=cax, pad=0.1,
                         label=r"$n_{3D}$ [$\SI{100}{\micro m^{-3}}$]")
            #mmfplt.imcontourf(x, r_new, px**2+pr**2,
            #                  aspect=1)
            #mmfplt.phase_contour(x, r, psi, linewidths=0.01,
            #                     alpha=0.2)

            ax.grid(lw=0.2, alpha=0.5, c='WhiteSmoke')

            plt.xlim(xlim)
            plt.ylim(ylim)
            ax.set_yticks([-2, 0, 2])
            #ax.set_yticklabels(["$-2$", "$\phantom{+}0$", "$\phantom{+}2$"])

            # Draw axial contours
            ax = ax_b
            plt.xlim(xlim)
            plt.sca(ax)
            #z = mmfplt.colors.color_complex(psi)
            mmfplt.imcontourf(x, r, n/n_unit, vmin=0, vmax=n_max/n_unit,
                              aspect=1)
            plt.colorbar(cax=cax, pad=0.1,
                         label=r"$n_{3D}$ [$\SI{100}{\micro m^{-3}}$]")
            #mmfplt.imcontourf(x, r_new, px**2+pr**2,
            #                  aspect=1)
            #mmfplt.phase_contour(x, r, psi, linewidths=0.01,
            #                     alpha=0.2)

            ax.grid(lw=0.2, alpha=0.5, c='WhiteSmoke')

            plt.xlim(xlim)
            plt.ylim(ylim)
            ax.set_yticks([-2, 0, 2])
            #ax.set_yticklabels(["$-2$", "$\phantom{+}0$", "$\phantom{+}2$"])

            if not stream:
                continue
            dx = 0.1
            dr = 0.1
            x0 = np.linspace(xlim[0], xlim[1], int(np.diff(xlim)/dx))[1:-1]
            r0 = np.linspace(r_new.min(), r_new.max(), int(np.diff(ylim)/dr))[1:-1]
            X0, R0 = np.meshgrid(x0, r0, sparse=False, indexing='ij')
            #plt.plot(X0.ravel(), R0.ravel(), 'x', c='w', ms=0.1)
            start_points = np.array([X0.ravel(), R0.ravel()]).T
            #start_points = np.array([(0.5, 0.5), (1.1, 1.1), (-1.1, 1.1)])
            #plt.plot(start_points[:, 0], start_points[:, 1], 'x', c='w', ms=0.1)
            ix = np.where(abs(x)<xlim[1])[0]
            res = plt.streamplot(x.ravel()[ix], r_new.ravel(),
                                 px[ix,:].T, pr[ix,:].T,
                                 density=(np.diff(xlim)/10/2, 1),
                                 #minlength=1,
                                 #maxlength=5,
                                 #integration_direction='both',
                                 #start_points=start_points,
                                 linewidth=0.1, arrowsize=0.1)

            print("Done")

        # Hide all tick labels for now:
        for axs in ax0s:
            for ax in axs:
                ax.tick_params(direction='in',
                               left=True, right=True,
                               bottom=True, top=True,
                               labelbottom=False, labelleft=False,
                               # length=6, width=2, colors='r',
                               # grid_color='r', grid_alpha=0.5)
                               )

        ax0s[0][0].tick_params(labelleft=True)
        ax0s[1][1].tick_params(labelleft=True)
        ax0s[1][-1].tick_params(labelleft=True, labelbottom=True)
        ax0s[3][-1].tick_params(labelbottom=True)

        # Labels
        x_label = r"$x$ [$\si{\micro m}$]"
        z_label = r"$z$ [$\si{\micro m}$]"
        n_label = r"$n_{1D}$ [$\si{nm^{-1}}$]$\qquad$"

        ax0s[0][0].set_ylabel(n_label)
        ax0s[1][1].set_ylabel(z_label)
        for ax in [ax0s[1][1], ax0s[3][1]]:
            ax.set_xlabel(x_label)

        for ax_, t_, xlim_, _label in zip(ax0s, t_s, xlims, "abcd"):
            ax = ax_[0]
            ax.text(xlim_[1]-0.5, 0.7*n1_max/n1_unit,
                    rf"$t=\SI{{{t_-10:g}}}{{ms}}$",
                    fontsize='small',
                    horizontalalignment='right')
            ax.text(xlim_[0]+1, 2.02,
                    rf"$\textbf{{{_label}}}$")

        return myfig

    def fig_comparison(self, t__waits=[6, 16, 26, 36], x_th_shift=1.0,
                       data=None):
        """Main comparison figure with experimental and numerical
        data."""

        # First calculate size so that images with fixed aspect ratio
        # fit correctly
        xlim = (-180, 180)
        ylim = (-50, 50)

        aspect_ratio = abs(np.diff(ylim)/np.diff(xlim))[0]
        rows = 4
        cols = 4
        space = 0.1             # Space between elements

        # Margins
        left = 0.1
        right = 0.04
        top = 0.05
        bottom = 0.1

        width = cols * 1 + (cols-1)*space
        height = (3*rows) * aspect_ratio + (cols-1)*space

        fig_width = width/(1 - left - right)
        fig_height = height/(1 - top - bottom)
        fig_aspect = fig_height/fig_width

        myfig = self.figure(
            num=2,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=f"{fig_aspect}*width",
            subplot_args=dict(left=left, bottom=bottom,
                              right=1.0-right, top=1.0-top),
        )

        x_unit = u.micron
        n_unit = 1./u.micron**2

        if data is None:
            # Experimental data
            from DATA import data_190424 as expt_nosoc
            from DATA import data_190425 as expt_soc
            expt_soc = {30: dict(data=expt_soc.data['C_R_fast_15uW.txt']),
                        60: dict(data=expt_soc.data['C_R_fast_30uW.txt']),
                        90: dict(data=expt_soc.data['C_R_fast_45uW.txt'])}
            expt_nosoc = {30: dict(data=expt_nosoc.data['C_R_fast_noSOC_15uW.txt']),
                          60: dict(data=expt_nosoc.data['C_R_fast_noSOC_30uW.txt']),
                          90: dict(data=expt_nosoc.data['C_R_fast_noSOC_45uW.txt'])}

            for key in expt_soc:
                _expt = expt_soc[key]
                ns = []
                for t__wait in t__waits:
                    x_, y_, n_ = _expt['data'].load(t_wait=t__wait, t_exp=10.1)
                    n_ = n_[0]


                    #####################
                    # Process experimental images - manually selecting
                    # regions of interest for plots, in pixels
                    image_extent = [250, 750, 50, 400] #should be same as ROI below

                    # Filters and detrends data
                    n_, (x_, y_) = data_init_base.process_image(n_.filled(-4000),
                                                                skip=20,
                                                                image_extent=image_extent,
                                                                x=x_, y=y_,
                                                                sentinel=-4000,
                                                                registration=False,
                                                                return_registration=False,
                                                                experimental_method=True,
                                                                detrend=True,)

                    x_offset = 4    # shifts left or right, in micron
                    y_0 = 106       # Midpoint of plot

                    # Region of Interest (ROI) box, in pixels
                    iL, iR, iD, iU = image_extent #250, 750, 50, 400
                    y_off = 500   # Uses a new ROI shifter up by y_off to
                                  # subtract off background (pixels)

                    # Select ROI
                    x = x_[iL:iR] - (x_[iL:iR][-1] + x_[iL:iR][0])/2 - x_offset
                    y = y_[iD:iU] - y_[iD:iU][0] - y_0

                    # Also for density, and subtract background
                    n_background = n_[iL:iR, iD+y_off:iU+y_off].mean(axis=1)[:, None]
                    n = n_[iL:iR, iD:iU] - n_background

                    # Subtract of thermal background
                    # CURRENTLY NOT IMPLMENTED, only works in 1D
                    #p0 = [260, 175, 100000, 0, 260, 300, 25000, 0]
                    #x, p_ = Moss.bimodalfit(n.sum(axis=1), p0, show=False)
                    #n -= Moss.gaussian(x,p_[4:8])[:,None]
                    #####################

                    ns.append(n)
                _expt['x'] = x
                _expt['y'] = y
                _expt['ns'] = ns

            for key in expt_nosoc:
                _expt = expt_nosoc[key]
                ns = []
                for t__wait in t__waits:
                    try:
                        x_, y_, n_ = _expt['data'].load(t_wait=t__wait, t_exp=10.1)
                        n_ = n_[0]
                    except KeyError:
                        n_ = 0*n_

                    #####################
                    # Process experimental images - manually selecting
                    # regions of interest for plots, in pixels
                    image_extent = [250, 750, 50, 400] #should be same as ROI below

                    # Filters and detrends data
                    n_, (x_, y_) = data_init_base.process_image(n_.filled(-4000),
                                                                skip=20,
                                                                image_extent=image_extent,
                                                                x=x_, y=y_,
                                                                sentinel=-4000,
                                                                registration=False,
                                                                return_registration=False,
                                                                experimental_method=True,
                                                                detrend=True,)


                    x_offset = 50    # shifts left or right, in micron
                    y_0 = 106       # Midpoint of plot

                    # Region of Interest (ROI) box, in pixels
                    iL, iR, iD, iU = image_extent
                    y_off = 500   # Uses a new ROI shifter up by y_off to
                                  # subtract off background (pixels)

                    # Select ROI
                    x = x_[iL:iR] - (x_[iL:iR][-1] + x_[iL:iR][0])/2 - x_offset
                    y = y_[iD:iU] - y_[iD:iU][0] - y_0

                    # Also for density, and subtract background
                    n_background = n_[iL:iR, iD+y_off:iU+y_off].mean(axis=1)[:, None]
                    n = n_[iL:iR, iD:iU] - n_background
                    #####################

                    ns.append(n)
                _expt['x'] = x
                _expt['y'] = y
                _expt['ns'] = ns

            # Simulations
            import runs_car_fast_final
            run = runs_car_fast_final.Run_IPG_xTF150()
            sims_soc = {
                30: dict(sim=run.simulations[0]),
                60: dict(sim=run.simulations[1]),
                90: dict(sim=run.simulations[2])
            }

            for key in [30, 60, 90]:
                sims = sims_soc[key]
                sim = sims['sim']
                e = sim.experiment
                assert sim.experiment.barrier_depth_nK == -key
                s0 = sim.get_state(t_=t__waits[0] + e.t__barrier, image=True)
                x, r = s0.xyz[:2]

                ns = []
                for t__wait in t__waits:
                    s = sim.get_state(t_=t__wait + e.t__barrier, image=True)
                    assert np.allclose(s.xyz[0], x)
                    assert np.allclose(s.xyz[1], r)

                    # Density of majority component
                    n_ = s.get_density()[0]

                    # Compute line-of-sight integral (Abel transform)
                    y = np.linspace(-r.max(), r.max(), 2*r.size)

                    # Remember - the basis does not know about lambda
                    n = s.basis.integrate2(n_, y/s.get_lambda())

                    # Join
                    #y = np.concatenate((-r[..., ::-1], r), axis=-1)
                    #n = np.concatenate((n_[..., ::-1], n_), axis=-1)

                    ns.append(n)
                sims['x'] = x
                sims['y'] = y
                sims['ns'] = ns

            data = dict(expt_nosoc=expt_nosoc, expt_soc=expt_soc, sims_soc=sims_soc)

        self._data_comparison = data
        expt_nosoc, expt_soc, sims_soc = (
            data['expt_nosoc'], data['expt_soc'], data['sims_soc'])

        myfig.ax.remove()       # Don't use base axis.
        grid0 = MPLGrid(fig=myfig.fig, subplot_spec=myfig.subplot_spec,
                        direction='right', share=True, space=space)
        keys = ['NoSOC', 30, 60, 90]
        x_label_key = 60

        for _k, key in enumerate(keys):
            grid1 = grid0.grid(direction='down', share=True, space=space)
            if key == "NoSOC":
                # Hack for now until we have NoSOC results
                key = keys[1]

            sims = sims_soc[key]
            expts = expt_soc[key]
            expts0 = expt_nosoc[key]
            x_t = sims['x']
            y_t = sims['y']
            ns_t = sims['ns']

            x_e = expts['x']
            y_e = expts['y']
            ns_e = expts['ns']

            x_e0 = expts0['x']
            y_e0 = expts0['y']
            ns_e0 = expts0['ns']

            for _t, t__wait in enumerate(t__waits):
                grid2 = grid1.grid(direction='down', space=0, share=True)

                ####################
                # Experiment: No SOC
                ax = ax_e0 = grid2.next()
                plt.sca(ax)

                if _t == 0:
                    # Top axis, label bucket depth
                    plt.title(fr"$U_{{\mathrm{{b}}}} = -\SI{{{key}}}{{nK}}$")
                ax.grid(False)
                ax.set_yticks([-40, 0, 40])
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                mmfplt.imcontourf(x_e0, y_e0, ns_e0[_t],
                                  aspect=1)

                # Add times
                if _k == len(keys)-1:
                    ax_e1.yaxis.set_label_position('right')
                    ax_e1.set_ylabel(
                        fr"$t_{{\mathrm{{wait}}}}=\SI{{{t__wait}}}{{ms}}$",
                        horizontalalignment='right', y=0.8,
                    )

                ####################
                # Experiment: SOC
                ax = ax_e1 = grid2.next()
                plt.sca(ax)

                ax.grid(False)
                ax.set_yticks([-40, 0, 40])
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                mmfplt.imcontourf(x_e, y_e, ns_e[_t],
                                  aspect=1)

                ####################
                # Theory: SOC
                ax = ax_t = grid2.next()
                plt.sca(ax)
                ax.grid(False)
                ax.set_yticks([-40, 0, 40])
                ax.set_xlim(xlim)
                ax.set_ylim(ylim)
                mmfplt.imcontourf(x_t/x_unit, y_t/x_unit,
                                  ns_t[_t]/n_unit,
                                  aspect=1)


                # Hide inner tick-lables
                for _ax in [ax_e0, ax_e1, ax_t]:
                    if _k > 0:
                        plt.setp(_ax.yaxis.get_ticklabels(), visible=False)
                    plt.setp(_ax.xaxis.get_ticklabels(), visible=False)

            # Last axis, include labels
            plt.setp(ax.xaxis.get_ticklabels(), visible=True)
            if key == x_label_key:
                plt.xlabel(r'$x$ [\si{\micro\meter}]')

        return myfig

        # Data organized by t_exp key here.
        data_exs = {}
        for key in data_180517.data:
            d = data_180517.data[key]
            t_exp = d.t_exps[0]
            if t_exps is not None and t_exp not in t_exps:
                continue
            if np.allclose(d.t_exps, t_exp):
                data_exs.setdefault(t_exp, []).append(d)
        for t_exp in data_exs:
            if not len(data_exs[t_exp]) == 1:
                print("t_exp={} has {} runs - using first"
                      .format(t_exp, len(data_exs[t_exp])))
            data_exs[t_exp] = data_exs[t_exp][0]

        data_exs = OrderedDict([(_t, data_exs[_t]) for _t in
                                sorted(data_exs)])

        import runs_final
        run = runs_final.RunSOCPaperSLOW()
        sims = {_s.experiment.rabi_frequency_E_R: _s for _s in run.simulations}

        # Get the no-SOC data.  The key here is rabi_frequency_E_R
        data_th = sims[0]

        # Again, keys are t_exp, data is (t_wait, x_ex, n_ex, x_th, n_th)
        txns = {_t: [] for _t in data_exs}

        for t_exp in data_exs:
            data_ex = data_exs[t_exp]
            ts_ex = data_ex.t_waits
            ns_ex = data_ex.get_1d_data().ns
            x_ex = data_ex.get_1d_data().x
            for _n, t_ in enumerate(ts_ex):
                try:
                    state = data_th.get_state(t_, image=t_exp)
                except IndexError:
                    print("t_={} not found".format(t_))
                    continue
                txns[t_exp].append(
                    (t_,
                     x_ex, ns_ex[_n],
                     state.xyz[0], state.get_density_x(mean=False).sum(axis=0)))

        grid = MPLGrid(fig=fig.fig, ax=fig.ax, direction='right', space=0.1)
        xlim = 100

        for t_exp in txns:
            subgrid = grid.grid(direction='down', share=True, space=0)
            for _n, (t_wait, x_ex, n_ex, x_th, n_th) in enumerate(txns[t_exp]):
                ax = subgrid.next()
                if _n == 0:
                    plt.title(r"t\_exp={}".format(t_exp))
                factor = np.trapz(n_th, x_th)/np.trapz(n_ex, x_ex)
                print(factor)
                ax.plot(x_th+x_th_shift, n_th, 'k-', lw=0.5, alpha=0.5)
                ax.plot(x_ex, n_ex*factor, 'r:', lw=0.5, alpha=0.5)
                ax.set_xlim(-xlim, xlim)
                ax.text(0.05, 0.7, 't={:g}ms'.format(t_wait),
                        transform=ax.transAxes)
        self._l = locals()
        return fig

    def fig_test(self):
        fig = self.figure(
            num=2,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=1.0,
            tight_layout=True,
        )

        grid = MPLGrid(fig=fig.fig, ax=fig.ax,
                       space=0)
        ax = grid.next()
        ax.plot([0,1],[0,1])
        ax = grid.next()
        ax.plot([0,1],[0,2])

        return fig

    def fig_comparison_no_soc(self, t_exps=[10], x_th_shift=1.0):
        fig = self.figure(
            num=2,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=1.0,
            tight_layout=False,
        )

        from DATA import data_180517

        # Data organized by t_exp key here.
        data_exs = {}
        for key in data_180517.data:
            d = data_180517.data[key]
            t_exp = d.t_exps[0]
            if t_exps is not None and t_exp not in t_exps:
                continue
            if np.allclose(d.t_exps, t_exp):
                data_exs.setdefault(t_exp, []).append(d)
        for t_exp in data_exs:
            if not len(data_exs[t_exp]) == 1:
                print("t_exp={} has {} runs - using first"
                      .format(t_exp, len(data_exs[t_exp])))
            data_exs[t_exp] = data_exs[t_exp][0]

        data_exs = OrderedDict([(_t, data_exs[_t]) for _t in
                                sorted(data_exs)])

        import runs_final
        run = runs_final.RunSOCPaperSLOW()
        sims = {_s.experiment.rabi_frequency_E_R: _s for _s in run.simulations}

        # Get the no-SOC data.  The key here is rabi_frequency_E_R
        data_th = sims[0]

        # Again, keys are t_exp, data is (t_wait, x_ex, n_ex, x_th, n_th)
        txns = {_t: [] for _t in data_exs}

        for t_exp in data_exs:
            data_ex = data_exs[t_exp]
            ts_ex = data_ex.t_waits
            ns_ex = data_ex.get_1d_data().ns
            x_ex = data_ex.get_1d_data().x
            for _n, t_ in enumerate(ts_ex):
                try:
                    state = data_th.get_state(t_, image=t_exp)
                except IndexError:
                    print("t_={} not found".format(t_))
                    continue
                txns[t_exp].append(
                    (t_,
                     x_ex, ns_ex[_n],
                     state.xyz[0], state.get_density_x().sum(axis=0)))

        grid = MPLGrid(fig=fig.fig, ax=fig.ax, direction='right', space=0.1)
        xlim = 100

        for t_exp in txns:
            subgrid = grid.grid(direction='down', share=True, space=0)
            for _n, (t_wait, x_ex, n_ex, x_th, n_th) in enumerate(txns[t_exp]):
                ax = subgrid.next()
                if _n == 0:
                    plt.title(r"t\_exp={}".format(t_exp))
                factor = np.trapz(n_th, x_th)/np.trapz(n_ex, x_ex)
                print(factor)
                ax.plot(x_th+x_th_shift, n_th, 'k-', lw=0.5, alpha=0.5)
                ax.plot(x_ex, n_ex*factor, 'r:', lw=0.5, alpha=0.5)
                ax.set_xlim(-xlim, xlim)
                ax.text(0.05, 0.7, 't={:g}ms'.format(t_wait),
                        transform=ax.transAxes)
        self._l = locals()
        return fig


    def fig_comparison_soc1(self, dx=-15, dOmega=0.0):
        fig = self.figure(
            num=3,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=1.0,
            tight_layout=False,
        )

        from DATA import data_180613

        # Data organized by t_exp key here.
        data_ex = {}
        key = 'C&R_slow.txt'
        d = data_180613.data[key]
        t_exp = 10.1


        Omega_E_R = 1.5

        assert np.allclose(d.t_exps, t_exp)
        E_R = data_180613.data_init_base.Units.E_R
        info = d.get_info()
        assert np.allclose(info.Omega, Omega_E_R*E_R)
        assert np.allclose(info.t_exp, t_exp)

        data_exs = OrderedDict([(Omega_E_R,  d)])

        import runs_final
        run = runs_final.RunSOCPaperSLOW()
        sims = {_s.experiment.rabi_frequency_E_R: _s for _s in run.simulations}

        # Key is Omega_E_R
        txns = {Omega_E_R: []}

        for Omega_E_R in data_exs:
            data_th = sims[Omega_E_R+dOmega]
            data_ex = data_exs[Omega_E_R]
            ts_ex = data_ex.t_waits
            ns_ex = data_ex.get_1d_data().ns
            x_ex = data_ex.get_1d_data().x
            for _n, t_ in enumerate(ts_ex):
                try:
                    state = data_th.get_state(t_, image=t_exp)
                except IndexError:
                    print("t_={} not found".format(t_))
                    continue
                txns[Omega_E_R].append(
                    (t_,
                     x_ex, ns_ex[_n],
                     state.xyz[0], state.get_density_x().sum(axis=0)))

        grid = MPLGrid(fig=fig.fig, ax=fig.ax, direction='right', space=0.1)
        xlim = 100

        for Omega_E_R in txns:
            subgrid = grid.grid(direction='down', share=True, space=0)
            for _n, (t_wait, x_ex, n_ex, x_th, n_th) in enumerate(txns[Omega_E_R]):
                ax = subgrid.next()
                if _n == 0:
                    plt.title(r"$\Omega={}E_R$".format(Omega_E_R))
                factor = np.trapz(n_th, x_th)/np.trapz(n_ex, x_ex)
                print(factor)
                ax.plot(x_th, n_th, 'k-', lw=0.5, alpha=0.5)
                ax.plot(x_ex-dx, n_ex*factor, 'r:', lw=0.5, alpha=0.5)
                ax.set_xlim(-xlim, xlim)
        self._l = locals()
        return fig

    def fig_comparison_soc2(self, dx=0):
        fig = self.figure(
            num=4,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=1.0,
            tight_layout=False,
        )

        from DATA import data_181104, data_181106

        # Data organized by t_exp key here.
        data_exs = {}
        key = 'C&R_slow.txt'
        data_exs[1.5] = data_181104.data['MMF.txt']
        data_exs[0.75] = data_181106.data['1106_0.75_CandR.txt']
        data_exs[2.25] = data_181106.data['1106_2.25_CandR.txt']

        t_exp = 10.1

        for Omega_E_R in data_exs:
            d = data_ex = data_exs[Omega_E_R]
            assert np.allclose(d.t_exps, t_exp)
            E_R =  data_181106.data_init_base.Units.E_R
            info = d.get_info()
            assert np.allclose(info.Omega, Omega_E_R*E_R)
            assert np.allclose(info.t_exp, t_exp)

        data_exs = OrderedDict([(Omega_E_R,  data_exs[Omega_E_R]) for
                                Omega_E_R in sorted(data_exs)])

        import runs_final
        if detuning:
            run = runs_final.RunSOCPaperSLOWdetuning()
            sims = {_s.experiment.rabi_frequency_E_R: _s for _s in run.simulations}
            sims[0] = runs_final.RunSOCPaperSLOW().simulations[0]
        else:
            run = runs_final.RunSOCPaperSLOW()
            sims = {_s.experiment.rabi_frequency_E_R: _s for _s in run.simulations}

        # Key is Omega_E_R
        txns = {Omega_E_R: [] for Omega_E_R in data_exs}

        for Omega_E_R in data_exs:
            data_th = sims[Omega_E_R]
            data_ex = data_exs[Omega_E_R]
            ts_ex = data_ex.t_waits
            ns_ex = data_ex.get_1d_data().ns
            x_ex = data_ex.get_1d_data().x
            for _n, t_ in enumerate(ts_ex):
                try:
                    state = data_th.get_state(t_, image=t_exp)
                except IndexError:
                    print("t_={} not found".format(t_))
                    continue
                txns[Omega_E_R].append(
                    (t_,
                     x_ex, ns_ex[_n],
                     state.xyz[0], state.get_density_x().sum(axis=0)))

        grid = MPLGrid(fig=fig.fig, ax=fig.ax, direction='right', space=0.1)
        xlim = 100

        for Omega_E_R in txns:
            subgrid = grid.grid(direction='down', share=True, space=0)
            for _n, (t_wait, x_ex, n_ex, x_th, n_th) in enumerate(txns[Omega_E_R]):
                ax = subgrid.next()
                if _n == 0:
                    plt.title(r"$\Omega={}E_R$".format(Omega_E_R))
                factor = np.trapz(n_th, x_th)/np.trapz(n_ex, x_ex)
                print(factor)
                ax.plot(x_th, n_th, 'k-', lw=0.5, alpha=0.5)
                ax.plot(x_ex-dx, n_ex*factor, 'r:', lw=0.5, alpha=0.5)
                ax.set_xlim(-xlim, xlim)
        self._l = locals()
        return fig


    def fig_comparison_fast(self, dx=0):
        fig = self.figure(
            num=4,              # If you want to redraw in the same window
            width='textwidth',  # For two-column documents, vs. 'textwidth'
            height=1.0,
            tight_layout=False,
        )

        from DATA import data_180129

        # Data organized by t_exp key here.
        data_exs = {}
        keys = ['C&R_FastLoad_24uW_1.txt',
                'C&R_FastLoad_24uW_2.txt',
                'C&R_FastLoad_24uW_3.txt']
        data_exs[1.5] = data_180129.data[keys[0]]

        t_exp = 10.1

        for Omega_E_R in data_exs:
            d = data_ex = data_exs[Omega_E_R]
            assert np.allclose(d.t_exps, t_exp)
            E_R =  data_180129.data_init_base.Units.E_R
            info = d.get_info()
            assert np.allclose(info.Omega, Omega_E_R*E_R)
            assert np.allclose(info.t_exp, t_exp)

        data_exs = OrderedDict([(Omega_E_R,  data_exs[Omega_E_R]) for
                                Omega_E_R in sorted(data_exs)])

        import runs_final
        run = runs_final.RunSOCPaperFAST()
        sims = {_s.experiment.rabi_frequency_E_R: _s for _s in run.simulations}

        # Key is Omega_E_R
        txns = {Omega_E_R: [] for Omega_E_R in data_exs}

        for Omega_E_R in data_exs:
            data_th = sims[Omega_E_R]
            data_ex = data_exs[Omega_E_R]
            ts_ex = data_ex.t_waits
            ns_ex = data_ex.get_1d_data().ns
            x_ex = data_ex.get_1d_data().x
            for _n, t_ in enumerate(ts_ex):
                try:
                    state = data_th.get_state(t_, image=t_exp)
                except IndexError:
                    print("t_={} not found".format(t_))
                    continue
                txns[Omega_E_R].append(
                    (t_,
                     x_ex, ns_ex[_n],
                     state.xyz[0], state.get_density_x().sum(axis=0)))

        grid = MPLGrid(fig=fig.fig, ax=fig.ax, direction='right', space=0.1)
        xlim = 100

        for Omega_E_R in txns:
            subgrid = grid.grid(direction='down', share=True, space=0)
            for _n, (t_wait, x_ex, n_ex, x_th, n_th) in enumerate(txns[Omega_E_R]):
                ax = subgrid.next()
                if _n == 0:
                    plt.title(r"$\Omega={}E_R$".format(Omega_E_R))
                factor = np.trapz(n_th, x_th)/np.trapz(n_ex, x_ex)
                print(factor)
                ax.plot(x_th, n_th, 'k-', lw=0.5, alpha=0.5)
                ax.plot(x_ex-dx, n_ex*factor, 'r:', lw=0.5, alpha=0.5)
                ax.set_xlim(-xlim, xlim)
        self._l = locals()
        return fig
'''
